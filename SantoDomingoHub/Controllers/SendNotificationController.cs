﻿using SantoDomingoHub.Clases;
using SantoDomingoHub.Models;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace SantoDomingoHub.Controllers
{
    public class SendNotificationController : ApiController
    {
        readonly string ConnectionStrings = "Endpoint=sb://notificationsantodomingo.servicebus.windows.net/;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=ehurbSnPwOa9qsAYQ3A4fyLvC7U2SWS2zBkEweKmrkM=";
        readonly string hubname = "notificationSantoDomingo";

        // POST: api/SendNotification
        public void Post([FromBody]Mensaje mensaje)
        {
            if (mensaje.Users != null || mensaje.Users.Count > 0)
            {
                List<String> users = new List<string>();

                foreach (var item in mensaje.Users)
                {
                    users.Add($"userId:{item}");
                }

                Hub.SendNotificationUserAsync(mensaje.Titulo, mensaje.Descripcion, ConnectionStrings, hubname, users);
            }
            else
            {
                Hub.SendNotificationAsync(mensaje.Titulo, mensaje.Descripcion, ConnectionStrings, hubname);
            }

            Ok();
        }
    }
}