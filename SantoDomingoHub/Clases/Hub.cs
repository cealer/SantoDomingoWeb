﻿using Microsoft.Azure.NotificationHubs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SantoDomingoHub.Clases
{
    public class Hub
    {
        public static NotificationHubClient hub;

        public static async void SendNotificationAsync(string title, string description, string connectionString, string hubName)
        {
            hub = NotificationHubClient.CreateClientFromConnectionString(connectionString, hubName);
            string payload = "{ \"data\" : {\"message\":\"" + title + "\"," + "\"Notification\":\"" + description + "\"}" + "}";
            await hub.SendGcmNativeNotificationAsync(payload);
        }

        public static async void SendNotificationUserAsync(string title, string description, string connectionString, string hubName, List<string> users)
        {
            hub = NotificationHubClient.CreateClientFromConnectionString(connectionString, hubName);
            string payload = "{ \"data\" : {\"message\":\"" + title + "\"," + "\"Notification\":\"" + description + "\"}" + "}";
            await hub.SendGcmNativeNotificationAsync(payload, users);
        }
    }
}