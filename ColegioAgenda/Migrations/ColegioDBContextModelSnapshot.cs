﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using ColegioAgenda.Models;

namespace ColegioAgenda.Migrations
{
    [DbContext(typeof(ColegioDBContext))]
    partial class ColegioDBContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.2")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("ColegioAgenda.Models.Alumno", b =>
                {
                    b.Property<string>("IdAlumno")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("char(8)");

                    b.Property<string>("Estado")
                        .IsRequired()
                        .HasColumnType("char(1)");

                    b.Property<string>("IdGradosSecciones")
                        .IsRequired()
                        .HasColumnType("char(8)");

                    b.Property<string>("IdPersona")
                        .IsRequired()
                        .HasColumnType("char(8)");

                    b.HasKey("IdAlumno")
                        .HasName("XPKAlumno");

                    b.HasIndex("IdGradosSecciones");

                    b.HasIndex("IdPersona");

                    b.ToTable("Alumno");
                });

            modelBuilder.Entity("ColegioAgenda.Models.AnioEscolar", b =>
                {
                    b.Property<string>("IdAnioEscolar")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("char(8)");

                    b.Property<string>("Anio")
                        .IsRequired()
                        .HasColumnType("char(4)");

                    b.Property<string>("Estado")
                        .IsRequired()
                        .HasColumnType("char(1)");

                    b.Property<string>("NombreAnio")
                        .HasColumnType("nvarchar(100)");

                    b.HasKey("IdAnioEscolar")
                        .HasName("XPKAnioEscolar");

                    b.ToTable("AnioEscolar");
                });

            modelBuilder.Entity("ColegioAgenda.Models.AsuntoAgenda", b =>
                {
                    b.Property<string>("IdAsuntoAgenda")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("char(18)");

                    b.Property<string>("Descripcion")
                        .IsRequired()
                        .HasMaxLength(200);

                    b.Property<string>("Estado")
                        .IsRequired()
                        .HasColumnType("char(1)");

                    b.Property<DateTime>("Fecha")
                        .HasColumnType("datetime");

                    b.Property<string>("IdAlumno")
                        .IsRequired()
                        .HasColumnType("char(8)");

                    b.Property<string>("IdEmisor")
                        .IsRequired();

                    b.Property<string>("IdReceptor");

                    b.Property<string>("Titulo")
                        .IsRequired()
                        .HasMaxLength(20);

                    b.Property<bool>("Visto");

                    b.HasKey("IdAsuntoAgenda")
                        .HasName("XPKAsunto_Agenda");

                    b.HasIndex("IdAlumno");

                    b.ToTable("Asunto_Agenda");
                });

            modelBuilder.Entity("ColegioAgenda.Models.Cursos", b =>
                {
                    b.Property<string>("IdCurso")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("char(8)");

                    b.Property<string>("Descripcion")
                        .IsRequired()
                        .HasMaxLength(100);

                    b.Property<string>("Estado")
                        .IsRequired()
                        .HasColumnType("char(1)");

                    b.HasKey("IdCurso")
                        .HasName("XPKCursos");

                    b.ToTable("Cursos");
                });

            modelBuilder.Entity("ColegioAgenda.Models.DetalleAsunto", b =>
                {
                    b.Property<string>("IdDetalleAsunto")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("char(18)");

                    b.Property<string>("Estado")
                        .IsRequired()
                        .HasColumnType("char(1)");

                    b.Property<string>("IdAsuntoAgenda")
                        .IsRequired()
                        .HasColumnType("char(18)");

                    b.Property<string>("IdEtiqueta")
                        .IsRequired()
                        .HasColumnType("char(8)");

                    b.HasKey("IdDetalleAsunto")
                        .HasName("XPKDetalle_Asunto");

                    b.HasIndex("IdAsuntoAgenda");

                    b.HasIndex("IdEtiqueta");

                    b.ToTable("Detalle_Asunto");
                });

            modelBuilder.Entity("ColegioAgenda.Models.DetalleCursos", b =>
                {
                    b.Property<string>("IdDetalleCursos")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("char(8)");

                    b.Property<string>("Estado")
                        .HasMaxLength(1);

                    b.Property<string>("IdAnioEscolar")
                        .HasColumnType("char(8)");

                    b.Property<string>("IdCurso")
                        .IsRequired()
                        .HasColumnType("char(8)");

                    b.Property<string>("IdGradosSecciones")
                        .IsRequired()
                        .HasColumnType("char(8)");

                    b.HasKey("IdDetalleCursos")
                        .HasName("XPKDetalle_Cursos_");

                    b.HasIndex("IdAnioEscolar");

                    b.HasIndex("IdCurso");

                    b.HasIndex("IdGradosSecciones");

                    b.ToTable("Detalle_Cursos");
                });

            modelBuilder.Entity("ColegioAgenda.Models.DetalleGradosSecciones", b =>
                {
                    b.Property<string>("IdGradosSecciones")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("char(8)");

                    b.Property<string>("Estado")
                        .IsRequired()
                        .HasColumnType("char(1)");

                    b.Property<string>("IdAnioEscolar")
                        .IsRequired()
                        .HasColumnType("char(8)");

                    b.Property<string>("IdGrado")
                        .IsRequired()
                        .HasColumnType("char(8)");

                    b.Property<string>("IdSeccion")
                        .HasColumnType("char(8)");

                    b.HasKey("IdGradosSecciones")
                        .HasName("XPKDetalle_Grados_Secciones");

                    b.HasIndex("IdAnioEscolar");

                    b.HasIndex("IdGrado");

                    b.HasIndex("IdSeccion");

                    b.ToTable("Detalle_Grados_Secciones");
                });

            modelBuilder.Entity("ColegioAgenda.Models.Etiqueta", b =>
                {
                    b.Property<string>("IdEtiqueta")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("char(8)");

                    b.Property<string>("Descripcion")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<string>("Estado")
                        .IsRequired()
                        .HasColumnType("char(1)");

                    b.HasKey("IdEtiqueta")
                        .HasName("XPKEtiqueta");

                    b.ToTable("Etiqueta");
                });

            modelBuilder.Entity("ColegioAgenda.Models.Grados", b =>
                {
                    b.Property<string>("IdGrado")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("char(8)");

                    b.Property<string>("Descripcion")
                        .HasColumnType("char(1)");

                    b.Property<string>("Estado")
                        .HasColumnType("char(1)");

                    b.Property<bool>("Primaria");

                    b.Property<bool>("Secundaria");

                    b.HasKey("IdGrado")
                        .HasName("XPKGrados");

                    b.ToTable("Grados");
                });

            modelBuilder.Entity("ColegioAgenda.Models.Personas", b =>
                {
                    b.Property<string>("IdPersona")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("char(8)");

                    b.Property<string>("ApellidoM")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<string>("ApellidoP")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<string>("Direccion")
                        .HasMaxLength(150);

                    b.Property<string>("Dni")
                        .IsRequired()
                        .HasColumnName("DNI")
                        .HasColumnType("char(8)");

                    b.Property<string>("Estado")
                        .IsRequired()
                        .HasColumnType("char(1)");

                    b.Property<DateTime>("FecNac")
                        .HasColumnType("datetime");

                    b.Property<string>("IdTipoPersona")
                        .HasColumnType("char(8)");

                    b.Property<string>("Nombres")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<string>("NumContacto")
                        .HasColumnType("char(9)");

                    b.Property<string>("Sexo")
                        .IsRequired()
                        .HasColumnType("char(1)");

                    b.HasKey("IdPersona")
                        .HasName("XPKPersonas");

                    b.HasIndex("IdTipoPersona");

                    b.ToTable("Personas");
                });

            modelBuilder.Entity("ColegioAgenda.Models.Profesor", b =>
                {
                    b.Property<string>("IdProfesor")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("char(8)");

                    b.Property<bool>("EsTutor");

                    b.Property<string>("Estado")
                        .IsRequired()
                        .HasColumnType("char(1)");

                    b.Property<string>("IdGradosSecciones")
                        .HasColumnType("char(8)");

                    b.Property<string>("IdPersona")
                        .IsRequired()
                        .HasColumnType("char(8)");

                    b.HasKey("IdProfesor")
                        .HasName("XPKProfesor");

                    b.HasIndex("IdGradosSecciones");

                    b.HasIndex("IdPersona");

                    b.ToTable("Profesor");
                });

            modelBuilder.Entity("ColegioAgenda.Models.Secciones", b =>
                {
                    b.Property<string>("IdSeccion")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("char(8)");

                    b.Property<string>("Descripcion")
                        .HasColumnType("char(1)");

                    b.Property<string>("Estado")
                        .HasColumnType("char(1)");

                    b.HasKey("IdSeccion")
                        .HasName("XPKSeccion");

                    b.ToTable("Secciones");
                });

            modelBuilder.Entity("ColegioAgenda.Models.TipoPersonas", b =>
                {
                    b.Property<string>("IdTipoPersona")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("char(8)");

                    b.Property<string>("Descripcion")
                        .IsRequired()
                        .HasMaxLength(25);

                    b.Property<string>("Estado")
                        .IsRequired()
                        .HasColumnType("char(1)");

                    b.HasKey("IdTipoPersona")
                        .HasName("XPKTipoPersona");

                    b.ToTable("TipoPersonas");
                });

            modelBuilder.Entity("ColegioAgenda.Models.Alumno", b =>
                {
                    b.HasOne("ColegioAgenda.Models.DetalleGradosSecciones", "IdGradosSeccionesNavigation")
                        .WithMany("Alumno")
                        .HasForeignKey("IdGradosSecciones");

                    b.HasOne("ColegioAgenda.Models.Personas", "IdPersonaNavigation")
                        .WithMany("Alumno")
                        .HasForeignKey("IdPersona");
                });

            modelBuilder.Entity("ColegioAgenda.Models.AsuntoAgenda", b =>
                {
                    b.HasOne("ColegioAgenda.Models.Alumno", "IdAlumnoNavigation")
                        .WithMany("AsuntoAgenda")
                        .HasForeignKey("IdAlumno")
                        .HasConstraintName("FK_Alumno_Asunt_Agen");
                });

            modelBuilder.Entity("ColegioAgenda.Models.DetalleAsunto", b =>
                {
                    b.HasOne("ColegioAgenda.Models.AsuntoAgenda", "IdAsuntoAgendaNavigation")
                        .WithMany("DetalleAsunto")
                        .HasForeignKey("IdAsuntoAgenda")
                        .HasConstraintName("FK_Asu_Agen_Det_Asun");

                    b.HasOne("ColegioAgenda.Models.Etiqueta", "IdEtiquetaNavigation")
                        .WithMany("DetalleAsunto")
                        .HasForeignKey("IdEtiqueta");
                });

            modelBuilder.Entity("ColegioAgenda.Models.DetalleCursos", b =>
                {
                    b.HasOne("ColegioAgenda.Models.AnioEscolar", "IdAnioEscolarNavigation")
                        .WithMany("DetalleCursos")
                        .HasForeignKey("IdAnioEscolar")
                        .HasConstraintName("FK_AnioEscolar_Det_Cur");

                    b.HasOne("ColegioAgenda.Models.Cursos", "IdCursoNavigation")
                        .WithMany("DetalleCursos")
                        .HasForeignKey("IdCurso")
                        .HasConstraintName("FK_Cursos_Detalle_Cursos");

                    b.HasOne("ColegioAgenda.Models.DetalleGradosSecciones", "IdGradosSeccionesNavigation")
                        .WithMany("DetalleCursos")
                        .HasForeignKey("IdGradosSecciones");
                });

            modelBuilder.Entity("ColegioAgenda.Models.DetalleGradosSecciones", b =>
                {
                    b.HasOne("ColegioAgenda.Models.AnioEscolar", "IdAnioEscolarNavigation")
                        .WithMany("DetalleGradosSecciones")
                        .HasForeignKey("IdAnioEscolar")
                        .HasConstraintName("FK_AnioEscolar_Det_Grad_Secc");

                    b.HasOne("ColegioAgenda.Models.Grados", "IdGradoNavigation")
                        .WithMany("DetalleGradosSecciones")
                        .HasForeignKey("IdGrado");

                    b.HasOne("ColegioAgenda.Models.Secciones", "IdSeccionNavigation")
                        .WithMany("DetalleGradosSecciones")
                        .HasForeignKey("IdSeccion");
                });

            modelBuilder.Entity("ColegioAgenda.Models.Personas", b =>
                {
                    b.HasOne("ColegioAgenda.Models.TipoPersonas", "IdTipoPersonaNavigation")
                        .WithMany("Personas")
                        .HasForeignKey("IdTipoPersona");
                });

            modelBuilder.Entity("ColegioAgenda.Models.Profesor", b =>
                {
                    b.HasOne("ColegioAgenda.Models.DetalleGradosSecciones", "IdGradosSeccionesNavigation")
                        .WithMany("Profesor")
                        .HasForeignKey("IdGradosSecciones")
                        .HasConstraintName("FK_Detalle_Grad_Secc_Pro");

                    b.HasOne("ColegioAgenda.Models.Personas", "IdPersonaNavigation")
                        .WithMany("Profesor")
                        .HasForeignKey("IdPersona")
                        .HasConstraintName("FK_Per_Pro");
                });
        }
    }
}
