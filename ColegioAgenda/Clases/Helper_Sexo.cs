﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ColegioAgenda.Clases
{
    public class Helper_Sexo
    {
        public List<Sexo> Sexos()
        {
            List<Sexo> lista = new List<Sexo>();
            lista.Add(new Sexo { Descripcion = "Masculino", Valor = "M" });
            lista.Add(new Sexo { Descripcion = "Femenino", Valor = "F" });
            return lista;
        }
    }
}
