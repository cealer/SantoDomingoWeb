﻿using ColegioAgenda.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ColegioAgenda.Clases
{
    public class Generador
    {
        private readonly ColegioDBContext _context;

        public Generador(ColegioDBContext context)
        {
            _context = context;

        }

        //Generar ID
        public async Task<string> GenerateIdAsync(string code, string Table)
        {
            string cod;
            using (var command = _context.Database.GetDbConnection().CreateCommand())
            {
                command.CommandText = $"SELECT '{code}'+RIGHT('000000' + RTRIM(LTRIM(STR(COUNT(*)+1))),6) FROM {Table}";
                _context.Database.OpenConnection();
                var r = await command.ExecuteScalarAsync();
                cod = r.ToString();
            }

            return cod;
        }
    }
}