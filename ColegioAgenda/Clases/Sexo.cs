﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ColegioAgenda.Clases
{
    public class Sexo
    {
        public string Descripcion { get; set; }
        public string Valor { get; set; }
    }
}
