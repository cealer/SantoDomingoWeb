﻿// Write your Javascript code.


function BuscarEtiqueta() {
    var table = document.getElementById("Tbody");

    while (table.rows.length > 0) {
        table.deleteRow(0);
    }
    var text = document.getElementById('tbxBuscarEtiqueta').value

    $.ajax({
        type: "GET",
        url: "/AsuntoAgendas/BuscarEtiquetas",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: { descripcion: text },
        success: function (data) {

            //alert(JSON.stringify(data));
            //$("#DIV").html('');

            $.each(data, function (i, item) {
                var rows = "<tr>" +
                    "<td>" + item.descripcion + "</td>" +
                    "<td>" + '<button  value="' + "" + item.idEtiqueta + '" onclick="AgregarEtiqueta(\'' + item.idEtiqueta + '\');"' + '>' + "AGREGAR</button>" + "</td>" +
                    "</tr>";
                $('#Table').append(rows);
            });

            console.log(data);
        },

        failure: function (data) {
            alert(data.responseText);
        },
        error: function (data) {
            alert(data.responseText);
        }

    });
}

function BuscarPersona() {
    var table = document.getElementById("TbodyPersona");

    while (table.rows.length > 0) {
        table.deleteRow(0);
    }

    var text = document.getElementById('tbxBuscarPersona').value

    $.ajax({
        type: "GET",
        url: "/AsuntoAgendas/BuscarPadreFamilia",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: { matricula: text },
        success: function (data) {

            //alert(JSON.stringify(data));
            //$("#DIV").html('');
            $.each(data.value, function (i, item) {

                var rows = "<tr>" +
                    "<td>" + item.surname + " " + item.givenName + "</td>" +
                    "<td>" + '<button  value="' + "" + item.objectId + '" onclick="AgregarPersona(\'' + item.objectId + "\'" + "," + "\'" + item.surname + " " + item.givenName + '\');"' + '>' + "AGREGAR</button>" + "</td>" +
                    "</tr>";


                $('#TablePersona').append(rows);

            });

            console.log(data);
        },

        failure: function (data) {
            alert(data.responseText);
        },
        error: function (data) {
            alert(data.responseText);
        }

    });
}

function BuscarAlumno() {
    var table = document.getElementById("TbodyAlumno");

    while (table.rows.length > 0) {
        table.deleteRow(0);
    }

    var text = document.getElementById('tbxBuscarAlumno').value

    $.ajax({
        type: "GET",
        url: "/AsuntoAgendas/BuscarAlumnoNombre",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: { datos: text },
        success: function (data) {

            $.each(data, function (i, item) {

                var rows = "<tr>" +
                    "<td>" + item.idPersonaNavigation.datos + "</td>" +
                    "<td>" + '<button  value="' + "" + "" + '" onclick="SeleccionarAlumno(\'' + item.matricula + "\'," + "\'" + item.idPersonaNavigation.datos +
                    "\'," + "\'" + item.idAlumno + '\');"' + '>' + "SELECCIONAR</button>" + "</td>" +
                    "</tr>";
                $('#TableAlumno').append(rows);
            });
            

            console.log(data);
        },

        failure: function (data) {
            alert(data.responseText);
        },
        error: function (data) {
            alert(data.responseText);
        }

    });
}

function QuitarEtiqueta(id) {
    $.ajax({
        type: "GET",
        url: "/AsuntoAgendas/QuitarEtiqueta",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: { etiqueta: id },
        success: function (data) {
        },
        failure: function (data) {
            alert("Fallo");
            alert(data.responseText);
        },
        error: function (data) {
            alert("Error");
            alert(data.responseText);
        }
    });
}

function AgregarPersona(id, datos) {
    $.ajax({
        type: "GET",
        url: "/AsuntoAgendas/AgregarPersona",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: { id: id, datos: datos },
        success: function (data) {

            var boton = '<a class="label-info" title="QUITAR"' +
                'onclick=alert("Elimnar");'
                + ">" + datos + "</a>&nbsp;";

            $("#PersonasListar").append(boton);
            $('#ModalPersona').modal('hide');
        },
        failure: function (data) {
            alert("Fallo");
            alert(data.responseText);
        },
        error: function (data) {
            alert("Error");
            alert(data.responseText);
        }
    });

}

function AgregarEtiqueta(id) {
    $.ajax({
        type: "GET",
        url: "/AsuntoAgendas/AgregarEtiqueta",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: { etiqueta: id },
        success: function (data) {

            var boton = '<a class="label-info" title="QUITAR"' +
                'onclick=alert("asd");'
                + ">" + data.descripcion + "</a>&nbsp;";
            $("#EtiquetasListar").append(boton);
            $('#MyModal').modal('hide');
        },
        failure: function (data) {
            alert("Fallo");
            alert(data.responseText);
        },
        error: function (data) {
            alert("Error");
            alert(data.responseText);
        }
    });

}

function CargarEtiquetas() {
    //$(document).ready(function () {

    var table = document.getElementById("Tbody");

    while (table.rows.length > 0) {
        table.deleteRow(0);
    }

    $.ajax({
        type: "GET",
        url: "/AsuntoAgendas/ObtenerEtiquetas",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {

            //alert(JSON.stringify(data));
            //$("#DIV").html('');

            $.each(data, function (i, item) {
                var rows = "<tr>" +
                    "<td>" + item.descripcion + "</td>" +
                    "<td>" + '<button  value="' + "" + item.idEtiqueta + '" onclick="AgregarEtiqueta(\'' + item.idEtiqueta + '\');"' + '>' + "AGREGAR</button>" + "</td>" +
                    "</tr>";
                $('#Table').append(rows);
            });

            console.log(data);
        },

        failure: function (data) {
            alert(data.responseText);
        },
        error: function (data) {
            alert(data.responseText);
        }

    });
    //});
}

function SeleccionarAlumno(matricula, nombres, idalumno) {
    document.getElementById("alumnoSeleccionado").value = nombres;
    document.getElementById("tbxBuscarPersona").value = matricula;
    document.getElementById("AsuntoAgenda_IdAlumno").value = idalumno;

    $('#ModalAlumno').modal('hide');
    BuscarPersona();
}

function CargarAlumnos() {

    var table = document.getElementById("TbodyAlumno");

    while (table.rows.length > 0) {
        table.deleteRow(0);
    }

    $.ajax({
        type: "GET",
        url: "/AsuntoAgendas/BuscarAlumno",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {

            $.each(data, function (i, item) {

                var rows = "<tr>" +
                    "<td>" + item.idPersonaNavigation.datos + "</td>" +
                    "<td>" + '<button  value="' + "" + "" + '" onclick="SeleccionarAlumno(\'' + item.matricula + "\'," + "\'" + item.idPersonaNavigation.datos +
                    "\'," + "\'" + item.idAlumno + '\');"' + '>' + "SELECCIONAR</button>" + "</td>" +
                    "</tr>";
                $('#TableAlumno').append(rows);
            });

            console.log(data);
        },

        failure: function (data) {
            alert(data.responseText);
        },
        error: function (data) {
            alert(data.responseText);
        }

    });
    //});
}

$(document).ready(function () {
    $('#btnCargarAlumno').click(function () {
        CargarAlumnos();
    });

});

$(document).ready(function () {
    $('#btnBuscarAlumno').click(function () {
        BuscarAlumno();
    });

});
