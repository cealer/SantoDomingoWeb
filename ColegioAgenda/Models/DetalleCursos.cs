﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ColegioAgenda.Models
{
    public partial class DetalleCursos
    {
        public string IdDetalleCursos { get; set; }

        [Display(Name = "Curso ")]
        public string IdCurso { get; set; }

        [Display(Name = "GradoSecciones ")]
        public string IdGradosSecciones { get; set; }

        [Display(Name = "AñoEscolar ")]
        public string IdAnioEscolar { get; set; }

        [MaxLength(1)]
        public string Estado { get; set; }

        public virtual AnioEscolar IdAnioEscolarNavigation { get; set; }
        public virtual Cursos IdCursoNavigation { get; set; }
        public virtual DetalleGradosSecciones IdGradosSeccionesNavigation { get; set; }
    }
}
