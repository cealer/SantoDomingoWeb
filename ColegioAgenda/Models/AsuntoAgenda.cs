﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ColegioAgenda.Models
{
    public partial class AsuntoAgenda
    {
        public AsuntoAgenda()
        {
            DetalleAsunto = new HashSet<DetalleAsunto>();
        }

        public string IdAsuntoAgenda { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [StringLength(20, ErrorMessage = "El campo {0} puede contener entre {2} a {1} caracteres", MinimumLength = 4)]
        [RegularExpression("^[a-zA-Z-ñÑ ]+$", ErrorMessage = "Solo debe ingresar letras.")]
        public string Titulo { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [StringLength(200, ErrorMessage = "El campo {0} puede contener entre {2} a {1} caracteres", MinimumLength = 4)]
        public string Descripcion { get; set; }

        [Display(Name = "A enviar")]
        public string IdReceptor { get; set; }
     
        public string IdEmisor { get; set; }

        [Display(Name = "Enviado por ")]
        public string Emisor { get; set; }

        [Display(Name = "A enviar ")]
        public string Receptor { get; set; }

        public bool Visto { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime Fecha { get; set; }

        [Display(Name = "Alumno")]
        public string IdAlumno { get; set; }

        public string Estado { get; set; }

        [JsonIgnore]
        public virtual ICollection<DetalleAsunto> DetalleAsunto { get; set; }

        [JsonIgnore]
        [Display(Name ="Alumno")]
        public virtual Alumno IdAlumnoNavigation { get; set; }
    }
}