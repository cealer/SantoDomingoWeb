﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace ColegioAgenda.Models
{
    public partial class ColegioDBContext : DbContext
    {
        public virtual DbSet<Alumno> Alumno { get; set; }
        public virtual DbSet<AnioEscolar> AnioEscolar { get; set; }
        public virtual DbSet<AsuntoAgenda> AsuntoAgenda { get; set; }
        public virtual DbSet<Cursos> Cursos { get; set; }
        public virtual DbSet<DetalleAsunto> DetalleAsunto { get; set; }
        public virtual DbSet<DetalleCursos> DetalleCursos { get; set; }
        public virtual DbSet<DetalleGradosSecciones> DetalleGradosSecciones { get; set; }
        public virtual DbSet<Etiqueta> Etiqueta { get; set; }
        public virtual DbSet<Grados> Grados { get; set; }
        public virtual DbSet<Personas> Personas { get; set; }
        public virtual DbSet<Profesor> Profesor { get; set; }
        public virtual DbSet<Secciones> Secciones { get; set; }
        public virtual DbSet<TipoPersonas> TipoPersonas { get; set; }

        public ColegioDBContext(DbContextOptions<ColegioDBContext> options)
    : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Alumno>(entity =>
            {
                entity.HasKey(e => e.IdAlumno)
                    .HasName("XPKAlumno");

                entity.Property(e => e.IdAlumno).HasColumnType("char(8)");

                entity.Property(e => e.Estado)
                    .IsRequired()
                    .HasColumnType("char(1)");

                entity.Property(e => e.IdGradosSecciones)
                    .IsRequired()
                    .HasColumnType("char(8)");

                entity.Property(e => e.IdPersona)
                    .IsRequired()
                    .HasColumnType("char(8)");

                entity.Property(e => e.Matricula).IsRequired().HasColumnType("nvarchar(50)");

                entity.HasOne(d => d.IdGradosSeccionesNavigation)
                    .WithMany(p => p.Alumno)
                    .HasForeignKey(d => d.IdGradosSecciones)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Det_Gra_Secc_Alu");

                entity.HasOne(d => d.IdPersonaNavigation)
                    .WithMany(p => p.Alumno)
                    .HasForeignKey(d => d.IdPersona)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Per_Alum");
            });

            modelBuilder.Entity<AnioEscolar>(entity =>
            {
                entity.HasKey(e => e.IdAnioEscolar)
                    .HasName("XPKAnioEscolar");

                entity.Property(e => e.IdAnioEscolar).HasColumnType("char(8)");

                entity.Property(e => e.Anio).HasColumnType("char(4)").IsRequired();

                entity.Property(e => e.Estado).HasColumnType("char(1)").IsRequired();

                entity.Property(e => e.NombreAnio).HasColumnType("nvarchar(100)");
            });

            modelBuilder.Entity<AsuntoAgenda>(entity =>
            {
                entity.HasKey(e => e.IdAsuntoAgenda)
                    .HasName("XPKAsunto_Agenda");

                entity.ToTable("Asunto_Agenda");

                entity.Property(e => e.IdAsuntoAgenda).HasColumnType("char(18)");

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.Estado)
                    .IsRequired()
                    .HasColumnType("char(1)");

                entity.Property(e => e.Fecha).HasColumnType("datetime");

                entity.Property(e => e.IdAlumno)
                    .IsRequired()
                    .HasColumnType("char(8)");

                entity.Property(e => e.IdReceptor);

                entity.Property(e => e.Receptor);

                entity.Property(e => e.Emisor);


                entity.Property(e=>e.IdEmisor).IsRequired();

                entity.Property(e => e.Titulo)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.HasOne(d => d.IdAlumnoNavigation)
                    .WithMany(p => p.AsuntoAgenda)
                    .HasForeignKey(d => d.IdAlumno)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Alumno_Asunt_Agen");

            });

            modelBuilder.Entity<Cursos>(entity =>
            {
                entity.HasKey(e => e.IdCurso)
                    .HasName("XPKCursos");

                entity.Property(e => e.IdCurso).HasColumnType("char(8)");

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Estado)
                    .IsRequired()
                    .HasColumnType("char(1)");
            });

            modelBuilder.Entity<DetalleAsunto>(entity =>
            {
                entity.HasKey(e => e.IdDetalleAsunto)
                    .HasName("XPKDetalle_Asunto");

                entity.ToTable("Detalle_Asunto");

                entity.Property(e => e.IdDetalleAsunto).HasColumnType("char(18)");

                entity.Property(e => e.Estado)
                    .IsRequired()
                    .HasColumnType("char(1)");

                entity.Property(e => e.IdAsuntoAgenda)
                    .IsRequired()
                    .HasColumnType("char(18)");

                entity.Property(e => e.IdEtiqueta)
                    .IsRequired()
                    .HasColumnType("char(8)");

                entity.HasOne(d => d.IdAsuntoAgendaNavigation)
                    .WithMany(p => p.DetalleAsunto)
                    .HasForeignKey(d => d.IdAsuntoAgenda)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Asu_Agen_Det_Asun");

                entity.HasOne(d => d.IdEtiquetaNavigation)
                    .WithMany(p => p.DetalleAsunto)
                    .HasForeignKey(d => d.IdEtiqueta)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Eti_Det_Asu");
            });

            modelBuilder.Entity<DetalleCursos>(entity =>
            {
                entity.HasKey(e => e.IdDetalleCursos)
                    .HasName("XPKDetalle_Cursos_");

                entity.ToTable("Detalle_Cursos");

                entity.Property(e => e.IdDetalleCursos).HasColumnType("char(8)");

                entity.Property(e => e.IdAnioEscolar).HasColumnType("char(8)");

                entity.Property(e => e.IdCurso)
                    .IsRequired()
                    .HasColumnType("char(8)");

                entity.Property(e => e.IdGradosSecciones)
                    .IsRequired()
                    .HasColumnType("char(8)");

                entity.HasOne(d => d.IdAnioEscolarNavigation)
                    .WithMany(p => p.DetalleCursos)
                    .HasForeignKey(d => d.IdAnioEscolar)
                    .HasConstraintName("FK_AnioEscolar_Det_Cur");

                entity.HasOne(d => d.IdCursoNavigation)
                    .WithMany(p => p.DetalleCursos)
                    .HasForeignKey(d => d.IdCurso)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Cursos_Detalle_Cursos");

                entity.HasOne(d => d.IdGradosSeccionesNavigation)
                    .WithMany(p => p.DetalleCursos)
                    .HasForeignKey(d => d.IdGradosSecciones)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Det_Grados_Secciones_Det_Cursos");
            });

            modelBuilder.Entity<DetalleGradosSecciones>(entity =>
            {
                entity.HasKey(e => e.IdGradosSecciones)
                    .HasName("XPKDetalle_Grados_Secciones");

                entity.ToTable("Detalle_Grados_Secciones");

                entity.Property(e => e.IdGradosSecciones).HasColumnType("char(8)");

                entity.Property(e => e.Estado)
                    .IsRequired()
                    .HasColumnType("char(1)");

                entity.Property(e => e.IdAnioEscolar)
                    .IsRequired()
                    .HasColumnType("char(8)");

                entity.Property(e => e.IdGrado)
                    .IsRequired()
                    .HasColumnType("char(8)");

                entity.Property(e => e.IdSeccion).HasColumnType("char(8)");

                entity.HasOne(d => d.IdAnioEscolarNavigation)
                    .WithMany(p => p.DetalleGradosSecciones)
                    .HasForeignKey(d => d.IdAnioEscolar)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_AnioEscolar_Det_Grad_Secc");

                entity.HasOne(d => d.IdGradoNavigation)
                    .WithMany(p => p.DetalleGradosSecciones)
                    .HasForeignKey(d => d.IdGrado)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Grados_Detalle_Grados_Secciones");

                entity.HasOne(d => d.IdSeccionNavigation)
                    .WithMany(p => p.DetalleGradosSecciones)
                    .HasForeignKey(d => d.IdSeccion)
                    .HasConstraintName("FK_Secciones_Detalle_Grados_Secciones");
            });

            modelBuilder.Entity<Etiqueta>(entity =>
            {
                entity.HasKey(e => e.IdEtiqueta)
                    .HasName("XPKEtiqueta");

                entity.Property(e => e.IdEtiqueta).HasColumnType("char(8)");

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Estado)
                    .IsRequired()
                    .HasColumnType("char(1)");
            });

            modelBuilder.Entity<Grados>(entity =>
            {
                entity.HasKey(e => e.IdGrado)
                    .HasName("XPKGrados");

                entity.Property(e => e.IdGrado).HasColumnType("char(8)");

                entity.Property(e => e.Descripcion).HasColumnType("char(1)");

                entity.Property(e => e.Estado).HasColumnType("char(1)");
            });

            modelBuilder.Entity<Personas>(entity =>
            {
                entity.HasKey(e => e.IdPersona)
                    .HasName("XPKPersonas");

                entity.Property(e => e.IdPersona).HasColumnType("char(8)");

                entity.Property(e => e.ApellidoM)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.ApellidoP)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Direccion).HasMaxLength(150);

                entity.Property(e => e.Dni)
                    .IsRequired()
                    .HasColumnName("DNI")
                    .HasColumnType("char(8)");

                entity.Property(e => e.Estado)
                    .IsRequired()
                    .HasColumnType("char(1)");

                entity.Property(e => e.FecNac).HasColumnType("datetime");

                entity.Property(e => e.IdTipoPersona)
                    .HasColumnType("char(8)");

                entity.Property(e => e.Nombres)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.NumContacto).HasColumnType("char(9)");

                entity.Property(e => e.Sexo)
                    .IsRequired()
                    .HasColumnType("char(1)");

                entity.HasOne(d => d.IdTipoPersonaNavigation)
                    .WithMany(p => p.Personas)
                    .HasForeignKey(d => d.IdTipoPersona)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_TipoPersonas_Persona");
            });

            modelBuilder.Entity<Profesor>(entity =>
            {
                entity.HasKey(e => e.IdProfesor)
                    .HasName("XPKProfesor");

                entity.Property(e => e.IdProfesor).HasColumnType("char(8)");

                entity.Property(e => e.Estado)
                    .IsRequired()
                    .HasColumnType("char(1)");

                entity.Property(e => e.IdGradosSecciones).HasColumnType("char(8)");

                entity.Property(e => e.IdPersona)
                    .IsRequired()
                    .HasColumnType("char(8)");

                entity.HasOne(d => d.IdGradosSeccionesNavigation)
                    .WithMany(p => p.Profesor)
                    .HasForeignKey(d => d.IdGradosSecciones)
                    .HasConstraintName("FK_Detalle_Grad_Secc_Pro");

                entity.HasOne(d => d.IdPersonaNavigation)
                    .WithMany(p => p.Profesor)
                    .HasForeignKey(d => d.IdPersona)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Per_Pro");
            });

            modelBuilder.Entity<Secciones>(entity =>
            {
                entity.HasKey(e => e.IdSeccion)
                    .HasName("XPKSeccion");

                entity.Property(e => e.IdSeccion).HasColumnType("char(8)");

                entity.Property(e => e.Descripcion).HasColumnType("char(1)");

                entity.Property(e => e.Estado).HasColumnType("char(1)");
            });

            modelBuilder.Entity<TipoPersonas>(entity =>
            {
                entity.HasKey(e => e.IdTipoPersona)
                    .HasName("XPKTipoPersona");

                entity.Property(e => e.IdTipoPersona).HasColumnType("char(8)");

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasMaxLength(25);

                entity.Property(e => e.Estado)
                    .IsRequired()
                    .HasColumnType("char(1)");
            });
        }
    }
}