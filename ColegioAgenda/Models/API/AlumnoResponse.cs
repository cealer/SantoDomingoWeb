﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ColegioAgenda.Models.API
{
    public class AlumnoResponse
    {
        public string IdPersona { get; set; }
        public string IdAlumno { get; set; }
        public string Nombres { get; set; }
        public string ApellidoP { get; set; }
        public string ApellidoM { get; set; }
        public string Dni { get; set; }
        public string Sexo { get; set; }
        public DateTime FecNac { get; set; }
        public string Grado { get; set; }
        public string Estado { get; set; }
    }
}