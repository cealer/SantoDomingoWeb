﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ColegioAgenda.Models.API
{
    public class PadreFamilia
    {
        public string Id { get; set; }
        public string Apellidos { get; set; }
        public string Nombres { get; set; }
    }
}