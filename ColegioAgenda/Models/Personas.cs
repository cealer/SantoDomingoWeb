﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace ColegioAgenda.Models
{
    public partial class Personas
    {
        public Personas()
        {
            Alumno = new HashSet<Alumno>();
            Profesor = new HashSet<Profesor>();
        }

        public string IdPersona { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [StringLength(50, ErrorMessage = "El campo {0} puede contener entre {2} a {1} caracteres", MinimumLength = 2)]
        [DataType(DataType.Text)]
        [RegularExpression("^[a-zA-Z-ñÑ ]+$", ErrorMessage = "Solo debe ingresar letras.")]
        public string Nombres { get; set; }

        [Display(Name = "Apellido Paterno")]
        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [StringLength(50, ErrorMessage = "El campo {0} puede contener entre {2} a {1} caracteres", MinimumLength = 2)]
        [DataType(DataType.Text)]
        [RegularExpression("^[a-zA-Z-ñÑ ]+$", ErrorMessage = "Solo debe ingresar letras.")]
        public string ApellidoP { get; set; }

        [Display(Name = "Apellido Materno")]
        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [StringLength(50, ErrorMessage = "El campo {0} puede contener entre {2} a {1} caracteres", MinimumLength = 2)]
        [DataType(DataType.Text)]
        [RegularExpression("^[a-zA-Z-ñÑ ]+$", ErrorMessage = "Solo debe ingresar letras.")]
        public string ApellidoM { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [StringLength(8, ErrorMessage = "El campo {0} puede contener entre {2} a {1} caracteres", MinimumLength = 8)]
        //[Index("DNIIndex", IsUnique = true)]
        [RegularExpression("^[0-9]*$", ErrorMessage = "El campo debe ser numérico")]
        public string Dni { get; set; }


        public string Sexo { get; set; }
        public string Direccion { get; set; }
        [Display(Name = "Celular/Telefono")]
        public string NumContacto { get; set; }
        public string Estado { get; set; }

        public string IdTipoPersona { get; set; }

        [Display(Name = "Fecha de nacimiento")]
        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime FecNac { get; set; }

        [NotMapped]
        [Display(Name = "Nombres y Apellidos")]
        public string Datos
        {
            get
            {
                return $"{ApellidoP} {ApellidoM}, {Nombres}";
            }
        }

        [JsonIgnore]
        public virtual ICollection<Alumno> Alumno { get; set; }
        [JsonIgnore]
        public virtual ICollection<Profesor> Profesor { get; set; }
        [Display(Name = "Tipo")]
        public virtual TipoPersonas IdTipoPersonaNavigation { get; set; }
    }
}