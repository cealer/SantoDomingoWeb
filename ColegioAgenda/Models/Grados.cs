﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ColegioAgenda.Models
{
    public partial class Grados
    {
        public Grados()
        {
            DetalleGradosSecciones = new HashSet<DetalleGradosSecciones>();
        }

        public string IdGrado { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [StringLength(1, ErrorMessage = "El campo {0} puede contener entre {2} a {1} caracteres", MinimumLength = 1)]
        [RegularExpression("^[0-9]*$", ErrorMessage = "El campo debe ser numérico")]
        public string Descripcion { get; set; }

        public bool Primaria { get; set; }
        public bool Secundaria { get; set; }
        public string Estado { get; set; }

        [NotMapped]
        [Display(Name = "Grado")]
        public string Detalle
        {
            get
            {

                if (Primaria == true)
                {
                    return $"{Descripcion} Primaria";
                }

                if (Secundaria == true)
                {
                    return $"{Descripcion} Secundaria";
                }
                return Descripcion;
            }
        }

        [JsonIgnore]
        public virtual ICollection<DetalleGradosSecciones> DetalleGradosSecciones { get; set; }
    }
}
