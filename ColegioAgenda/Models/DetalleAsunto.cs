﻿using System;
using System.Collections.Generic;

namespace ColegioAgenda.Models
{
    public partial class DetalleAsunto
    {
        public string IdDetalleAsunto { get; set; }
        public string IdAsuntoAgenda { get; set; }
        public string IdEtiqueta { get; set; }
        public string Estado { get; set; }

        public virtual AsuntoAgenda IdAsuntoAgendaNavigation { get; set; }
        public virtual Etiqueta IdEtiquetaNavigation { get; set; }
    }
}
