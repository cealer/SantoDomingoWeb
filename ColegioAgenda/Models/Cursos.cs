﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ColegioAgenda.Models
{
    public partial class Cursos
    {
        public Cursos()
        {
            DetalleCursos = new HashSet<DetalleCursos>();
        }
        [Key]
        public string IdCurso { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [StringLength(50, ErrorMessage = "El campo {0} puede contener entre {2} a {1} caracteres", MinimumLength = 4)]
        [DataType(DataType.Text)]
        [RegularExpression("^[a-zA-Z-ñÑ ]+$", ErrorMessage = "Solo debe ingresar letras.")]
        public string Descripcion { get; set; }

        public string Estado { get; set; }

        public virtual ICollection<DetalleCursos> DetalleCursos { get; set; }
    }
}
