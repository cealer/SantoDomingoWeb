﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ColegioAgenda.Models
{
    public partial class AnioEscolar
    {
        public AnioEscolar()
        {
            DetalleCursos = new HashSet<DetalleCursos>();
            DetalleGradosSecciones = new HashSet<DetalleGradosSecciones>();
        }

        public string IdAnioEscolar { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [StringLength(50, ErrorMessage = "El campo {0} puede contener entre {2} a {1} caracteres", MinimumLength = 4)]
        [DataType(DataType.Text)]
        [RegularExpression("^[a-zA-Z-ñÑ ]+$", ErrorMessage = "Solo debe ingresar letras.")]
        [Display(Name = "NombreAño")]
        public string NombreAnio { get; set; }

        [StringLength(4, ErrorMessage = "El campo {0} puede contener entre {2} a {1} caracteres", MinimumLength = 4)]
        [RegularExpression("^[0-9]*$", ErrorMessage = "El campo debe ser numérico")]
        [Display(Name = "Año")]
        public string Anio { get; set; }

        public string Estado { get; set; }
        [JsonIgnore]
        public virtual ICollection<DetalleCursos> DetalleCursos { get; set; }
        [JsonIgnore]
        public virtual ICollection<DetalleGradosSecciones> DetalleGradosSecciones { get; set; }
    }
}