﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ColegioAgenda.Models
{
    public partial class DetalleGradosSecciones
    {
        public DetalleGradosSecciones()
        {
            Alumno = new HashSet<Alumno>();
            DetalleCursos = new HashSet<DetalleCursos>();
            Profesor = new HashSet<Profesor>();
        }

        public string IdGradosSecciones { get; set; }

        [Display(Name = "Grado ")]
        public string IdGrado { get; set; }

        [Display(Name = "Sección ")]
        public string IdSeccion { get; set; }
        public string Estado { get; set; }

        [Display(Name = "AñoEscolar ")]
        public string IdAnioEscolar { get; set; }

        [NotMapped]
        [Display(Name = "Grados|Sección")]
        public string Descripcion
        {
            get
            {
                if (IdGradoNavigation != null && IdAnioEscolarNavigation != null)
                {
                    if (IdSeccionNavigation != null)
                    {
                        return $"{IdAnioEscolarNavigation.Anio} {IdGradoNavigation.Detalle} {IdSeccionNavigation.Descripcion}";
                    }
                    return $"{IdAnioEscolarNavigation.Anio} {IdGradoNavigation.Detalle}";
                }

                return $"";
            }
        }

        [JsonIgnore]
        public virtual ICollection<Alumno> Alumno { get; set; }
        [JsonIgnore]
        public virtual ICollection<DetalleCursos> DetalleCursos { get; set; }
        [JsonIgnore]
        public virtual ICollection<Profesor> Profesor { get; set; }

        public virtual AnioEscolar IdAnioEscolarNavigation { get; set; }
        public virtual Grados IdGradoNavigation { get; set; }
        [JsonIgnore]
        public virtual Secciones IdSeccionNavigation { get; set; }
    }
}