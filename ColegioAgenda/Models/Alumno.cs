﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ColegioAgenda.Models
{
    public partial class Alumno
    {
        public Alumno()
        {
            AsuntoAgenda = new HashSet<AsuntoAgenda>();
        }

        public string IdAlumno { get; set; }

        [Display(Name = "Grado")]
        public string IdGradosSecciones { get; set; }

        [Display(Name = "Alumno")]
        public string IdPersona { get; set; }

        [Display(Name = "Matrícula")]
        public string Matricula { get; set; }

        //[Required]
        public string Estado { get; set; }

        [JsonIgnore]
        public virtual ICollection<AsuntoAgenda> AsuntoAgenda { get; set; }
        [JsonIgnore]
        [Display(Name = "Grado")]
        public virtual DetalleGradosSecciones IdGradosSeccionesNavigation { get; set; }
        [Display(Name = "Alumno")]
        public virtual Personas IdPersonaNavigation { get; set; }
    }
}
