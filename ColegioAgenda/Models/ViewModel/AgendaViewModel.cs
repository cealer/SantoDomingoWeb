﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ColegioAgenda.Models.ViewModel
{
    public class AgendaViewModel
    {
        public AsuntoAgenda AsuntoAgenda { get; set; }
        public List<Etiqueta> ListaEtiquetas { get; set; }
    }
}