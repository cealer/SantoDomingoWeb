﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ColegioAgenda.Models.ViewModel
{
    public class AlumnoViewModel
    {
        public Personas Persona { get; set; }
        public Alumno Alumno { get; set; }
    }
}