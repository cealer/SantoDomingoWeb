﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ColegioAgenda.Models.ViewModel
{
    public class ProfesorViewModel
    {
        public Personas Persona { get; set; }
        public Profesor Profesor { get; set; }
    }
}