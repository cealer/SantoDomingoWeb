﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ColegioAgenda.Models.ViewModel
{
    public class Emisor
    {
        public string Id { get; set; }
        public string Datos { get; set; }
    }
}