﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ColegioAgenda.Models
{
    public partial class Profesor
    {
        public string IdProfesor { get; set; }
        public string IdGradosSecciones { get; set; }
        public string IdPersona { get; set; }
        public string Estado { get; set; }
        public bool EsTutor { get; set; }

        [Display(Name ="Grado")]
        public virtual DetalleGradosSecciones IdGradosSeccionesNavigation { get; set; }
        [Display(Name ="Profesor")]
        public virtual Personas IdPersonaNavigation { get; set; }
    }
}