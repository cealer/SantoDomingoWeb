﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ColegioAgenda.Models
{
    public partial class TipoPersonas
    {
        public TipoPersonas()
        {
            Personas = new HashSet<Personas>();
        }

        public string IdTipoPersona { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [StringLength(50, ErrorMessage = "El campo {0} puede contener entre {2} a {1} caracteres", MinimumLength = 3)]
        [DataType(DataType.Text)]
        [RegularExpression("^[a-zA-Z-ñÑ ]+$", ErrorMessage = "Solo debe ingresar letras.")]
        public string Descripcion { get; set; }

        public string Estado { get; set; }

        public virtual ICollection<Personas> Personas { get; set; }
    }
}
