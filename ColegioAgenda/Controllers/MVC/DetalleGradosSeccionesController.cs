using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ColegioAgenda.Models;
using ColegioAgenda.Clases;
using Microsoft.AspNetCore.Authorization;

namespace ColegioAgenda.Controllers.MVC
{
    [RequireHttps]
    [Authorize]
    public class DetalleGradosSeccionesController : Controller
    {
        private readonly ColegioDBContext _context;
        private readonly Generador generador;

        public DetalleGradosSeccionesController(ColegioDBContext context)
        {
            _context = context;
            generador = new Generador(context);
        }

        // GET: DetalleGradosSecciones
        public async Task<IActionResult> Index()
        {
            var colegioDBContext = _context.DetalleGradosSecciones.Where(x => x.Estado == "1").Include(d => d.IdAnioEscolarNavigation).Include(d => d.IdGradoNavigation).Include(d => d.IdSeccionNavigation).OrderBy(x => x.IdGradoNavigation.Detalle);
            return View(await colegioDBContext.ToListAsync());
        }

        // GET: DetalleGradosSecciones/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var detalleGradosSecciones = await _context.DetalleGradosSecciones
                .Include(d => d.IdAnioEscolarNavigation)
                .Include(d => d.IdGradoNavigation)
                .Include(d => d.IdSeccionNavigation)
                .SingleOrDefaultAsync(m => m.IdGradosSecciones == id);
            if (detalleGradosSecciones == null)
            {
                return NotFound();
            }

            return View(detalleGradosSecciones);
        }

        // GET: DetalleGradosSecciones/Create
        public IActionResult Create()
        {
            ViewData["IdAnioEscolar"] = new SelectList(_context.AnioEscolar.Where(x => x.Estado == "1"), "IdAnioEscolar", "Anio");
            ViewData["IdGrado"] = new SelectList(_context.Grados.Where(x => x.Estado == "1").OrderBy(x => x.Detalle), "IdGrado", "Detalle");
            ViewData["IdSeccion"] = new SelectList(_context.Secciones.Where(x => x.Estado == "1"), "IdSeccion", "Descripcion");
            return View();
        }

        // POST: DetalleGradosSecciones/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdGradosSecciones,IdGrado,IdSeccion,Estado,IdAnioEscolar")] DetalleGradosSecciones detalleGradosSecciones)
        {
            if (ModelState.IsValid)
            {
                detalleGradosSecciones.Estado = "1";
                detalleGradosSecciones.IdGradosSecciones = generador.GenerateIdAsync("GS", "Detalle_Grados_Secciones").Result;
                _context.Add(detalleGradosSecciones);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewData["IdAnioEscolar"] = new SelectList(_context.AnioEscolar.Where(x => x.Estado == "1"), "IdAnioEscolar", "Anio", detalleGradosSecciones.IdAnioEscolar);
            ViewData["IdGrado"] = new SelectList(_context.Grados.Where(x => x.Estado == "1"), "IdGrado", "Detalle", detalleGradosSecciones.IdGrado);
            ViewData["IdSeccion"] = new SelectList(_context.Secciones.Where(x => x.Estado == "1"), "IdSeccion", "Descripcion", detalleGradosSecciones.IdSeccion);
            return View(detalleGradosSecciones);
        }

        // GET: DetalleGradosSecciones/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var detalleGradosSecciones = await _context.DetalleGradosSecciones.SingleOrDefaultAsync(m => m.IdGradosSecciones == id);
            if (detalleGradosSecciones == null)
            {
                return NotFound();
            }
            ViewData["IdAnioEscolar"] = new SelectList(_context.AnioEscolar.Where(x => x.Estado == "1"), "IdAnioEscolar", "Anio", detalleGradosSecciones.IdAnioEscolar);
            ViewData["IdGrado"] = new SelectList(_context.Grados.Where(x => x.Estado == "1"), "IdGrado", "Detalle", detalleGradosSecciones.IdGrado);
            ViewData["IdSeccion"] = new SelectList(_context.Secciones.Where(x => x.Estado == "1"), "IdSeccion", "Descripcion", detalleGradosSecciones.IdSeccion);
            return View(detalleGradosSecciones);
        }

        // POST: DetalleGradosSecciones/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("IdGradosSecciones,IdGrado,IdSeccion,Estado,IdAnioEscolar")] DetalleGradosSecciones detalleGradosSecciones)
        {
            if (id != detalleGradosSecciones.IdGradosSecciones)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(detalleGradosSecciones);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DetalleGradosSeccionesExists(detalleGradosSecciones.IdGradosSecciones))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            ViewData["IdAnioEscolar"] = new SelectList(_context.AnioEscolar.Where(x => x.Estado == "1"), "IdAnioEscolar", "Anio", detalleGradosSecciones.IdAnioEscolar);
            ViewData["IdGrado"] = new SelectList(_context.Grados.Where(x => x.Estado == "1"), "IdGrado", "Detalle", detalleGradosSecciones.IdGrado);
            ViewData["IdSeccion"] = new SelectList(_context.Secciones.Where(x => x.Estado == "1"), "IdSeccion", "Descripcion", detalleGradosSecciones.IdSeccion);
            return View(detalleGradosSecciones);
        }

        // GET: DetalleGradosSecciones/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var detalleGradosSecciones = await _context.DetalleGradosSecciones
                .Include(d => d.IdAnioEscolarNavigation)
                .Include(d => d.IdGradoNavigation)
                .Include(d => d.IdSeccionNavigation)
                .SingleOrDefaultAsync(m => m.IdGradosSecciones == id);
            if (detalleGradosSecciones == null)
            {
                return NotFound();
            }

            return View(detalleGradosSecciones);
        }

        // POST: DetalleGradosSecciones/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var detalleGradosSecciones = await _context.DetalleGradosSecciones.SingleOrDefaultAsync(m => m.IdGradosSecciones == id);
            detalleGradosSecciones.Estado = "0";
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool DetalleGradosSeccionesExists(string id)
        {
            return _context.DetalleGradosSecciones.Any(e => e.IdGradosSecciones == id);
        }
    }
}
