using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ColegioAgenda.Models;
using ColegioAgenda.Clases;
using Microsoft.AspNetCore.Authorization;

namespace ColegioAgenda.Controllers.MVC
{
    [RequireHttps]
    [Authorize]
    public class AnioEscolarsController : Controller
    {
        private readonly ColegioDBContext _context;
        private readonly Generador generador;

        public AnioEscolarsController(ColegioDBContext context)
        {
            _context = context;
            generador = new Generador(context);
        }

        // GET: AnioEscolars
        public async Task<IActionResult> Index()
        {
            return View(await _context.AnioEscolar.Where(x => x.Estado == "1").ToListAsync());
        }

        // GET: AnioEscolars/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var anioEscolar = await _context.AnioEscolar
                .SingleOrDefaultAsync(m => m.IdAnioEscolar == id);
            if (anioEscolar == null)
            {
                return NotFound();
            }

            return View(anioEscolar);
        }

        // GET: AnioEscolars/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: AnioEscolars/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdAnioEscolar,NombreAnio,Anio,Estado")] AnioEscolar anioEscolar)
        {
            if (ModelState.IsValid)
            {
                anioEscolar.Estado = "1";
                anioEscolar.IdAnioEscolar = generador.GenerateIdAsync("AE", "AnioEscolar").Result;
                _context.Add(anioEscolar);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(anioEscolar);
        }

        // GET: AnioEscolars/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var anioEscolar = await _context.AnioEscolar.SingleOrDefaultAsync(m => m.IdAnioEscolar == id);
            if (anioEscolar == null)
            {
                return NotFound();
            }
            return View(anioEscolar);
        }

        // POST: AnioEscolars/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("IdAnioEscolar,NombreAnio,Anio,Estado")] AnioEscolar anioEscolar)
        {
            if (id != anioEscolar.IdAnioEscolar)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(anioEscolar);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AnioEscolarExists(anioEscolar.IdAnioEscolar))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            return View(anioEscolar);
        }

        // GET: AnioEscolars/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var anioEscolar = await _context.AnioEscolar
                .SingleOrDefaultAsync(m => m.IdAnioEscolar == id);
            if (anioEscolar == null)
            {
                return NotFound();
            }

            return View(anioEscolar);
        }

        // POST: AnioEscolars/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var anioEscolar = await _context.AnioEscolar.SingleOrDefaultAsync(m => m.IdAnioEscolar == id);
            anioEscolar.Estado = "0";
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool AnioEscolarExists(string id)
        {
            return _context.AnioEscolar.Any(e => e.IdAnioEscolar == id);
        }
    }
}