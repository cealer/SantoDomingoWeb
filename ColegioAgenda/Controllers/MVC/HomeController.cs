﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using ColegioAgenda.Models;
using Newtonsoft.Json;
using ColegioAgenda.Azure.B2C;

namespace ColegioAgenda.Controllers.MVC
{
    [RequireHttps]
    public class HomeController : Controller
    {
        private readonly ColegioDBContext _context;
        
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View();
        }

        public HomeController(ColegioDBContext context)
        {
            _context = context;
        }

        [Route("admin")]
        [Authorize]
        public IActionResult HomeAdmin()
            {
            var matricula = User.FindFirst("extension_Matricula");
            var dni = User.FindFirst("extension_DNI");

            if (dni != null)
            {
                var rDni = _context.Personas.Where(x => x.Dni.StartsWith(dni.Value));
                //Comprobar si existe el dni dentro del personal de la empresa.
                if (rDni != null && rDni.Count() > 0)
                {
                    return View();
                }
            }

            if (matricula != null)
            {
                var rMatricula = _context.Alumno.Where(x => x.Matricula.StartsWith(matricula.Value));

                //Mostrar Que el padre de familia no puede ingresar a esta web app
                if (rMatricula != null && rMatricula.Count() > 0)
                {
                    return RedirectToAction("SignOut", "Session");
                }
            }

            return RedirectToAction("SignOut", "Session");
        }

        public IActionResult HomeInformativo()
        {
            return View();
        }
    }
}