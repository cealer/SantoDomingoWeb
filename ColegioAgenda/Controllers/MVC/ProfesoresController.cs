using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ColegioAgenda.Models;
using ColegioAgenda.Clases;
using ColegioAgenda.Models.ViewModel;
using Microsoft.AspNetCore.Authorization;

namespace ColegioAgenda.Controllers.MVC
{
    [RequireHttps]
    [Authorize]
    public class ProfesoresController : Controller
    {
        private readonly ColegioDBContext _context;
        private readonly Generador generador;
        private readonly Helper_Sexo helper_Sexo;

        public ProfesoresController(ColegioDBContext context)
        {
            _context = context;
            generador = new Generador(context);
            helper_Sexo = new Helper_Sexo();
        }

        // GET: Profesores
        public async Task<IActionResult> Index()
        {
            var colegioDBContext = _context.Profesor.Where(x => x.Estado == "1").Include(p => p.IdGradosSeccionesNavigation).Include(x=>x.IdGradosSeccionesNavigation.IdGradoNavigation).Include(x => x.IdGradosSeccionesNavigation.IdSeccionNavigation).Include(p => p.IdPersonaNavigation);
            return View(await colegioDBContext.ToListAsync());
        }

        // GET: Profesores/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var profesor = await _context.Profesor
                .Include(p => p.IdGradosSeccionesNavigation)
                .Include(p => p.IdPersonaNavigation)
                .SingleOrDefaultAsync(m => m.IdProfesor == id);
            if (profesor == null)
            {
                return NotFound();
            }

            return View(profesor);
        }

        // GET: Profesores/Create
        public IActionResult Create()
        {
            ViewData["IdGradosSecciones"] = new SelectList(_context.DetalleGradosSecciones.Where(x => x.Estado == "1").Include(x => x.IdGradoNavigation).Include(x => x.IdSeccionNavigation).Include(x => x.IdAnioEscolarNavigation), "IdGradosSecciones", "Descripcion","Seleccionar");
            ViewData["Sexo"] = new SelectList(helper_Sexo.Sexos(), "Valor", "Descripcion");
            return View();
        }

        // POST: Profesores/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(ProfesorViewModel profesorViewModel)
        {
            if (ModelState.IsValid)
            {
                profesorViewModel.Persona.IdPersona = generador.GenerateIdAsync("PE", "Personas").Result;
                profesorViewModel.Profesor.IdProfesor = generador.GenerateIdAsync("PF", "Profesor").Result;

                var trasaction = _context.Database.BeginTransaction();
                try
                {
                    profesorViewModel.Persona.Estado = "1";
                    _context.Personas.Add(profesorViewModel.Persona);
                    profesorViewModel.Profesor.Estado = "1";
                    profesorViewModel.Profesor.IdPersona = profesorViewModel.Persona.IdPersona;
                    _context.Profesor.Add(profesorViewModel.Profesor);
                    await _context.SaveChangesAsync();
                    trasaction.Commit();
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    trasaction.Rollback();
                }
            }
            ViewData["IdGradosSecciones"] = new SelectList(_context.DetalleGradosSecciones.Where(x => x.Estado == "1").Include(x => x.IdGradoNavigation).Include(x => x.IdSeccionNavigation).Include(x => x.IdAnioEscolarNavigation), "IdGradosSecciones", "Descripcion", "Seleccionar");
            ViewData["Sexo"] = new SelectList(helper_Sexo.Sexos(), "Valor", "Descripcion");
            return View(profesorViewModel);
        }

        // GET: Profesores/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var profesor = await _context.Profesor.SingleOrDefaultAsync(m => m.IdProfesor == id);
            var persona = await _context.Personas.SingleOrDefaultAsync(m => m.IdPersona == profesor.IdPersona);
            ProfesorViewModel profesorViewModel = new ProfesorViewModel();
            profesorViewModel.Persona = persona;
            profesorViewModel.Profesor = profesor;
            if (profesor == null)
            {
                return NotFound();
            }
            ViewData["IdGradosSecciones"] = new SelectList(_context.DetalleGradosSecciones.Where(x => x.Estado == "1").Include(x => x.IdGradoNavigation).Include(x => x.IdSeccionNavigation).Include(x => x.IdAnioEscolarNavigation), "IdGradosSecciones", "Descripcion", "Seleccionar");
            ViewData["Sexo"] = new SelectList(helper_Sexo.Sexos(), "Valor", "Descripcion");
            return View(profesorViewModel);
        }

        // POST: Profesores/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, ProfesorViewModel profesorViewModel)
        {
            if (id != profesorViewModel.Profesor.IdProfesor)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(profesorViewModel.Persona);
                    _context.Update(profesorViewModel.Profesor);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProfesorExists(profesorViewModel.Profesor.IdProfesor))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            ViewData["IdGradosSecciones"] = new SelectList(_context.DetalleGradosSecciones.Where(x => x.Estado == "1").Include(x => x.IdGradoNavigation).Include(x => x.IdSeccionNavigation).Include(x => x.IdAnioEscolarNavigation), "IdGradosSecciones", "Descripcion", "Seleccionar");
            ViewData["Sexo"] = new SelectList(helper_Sexo.Sexos(), "Valor", "Descripcion");
            return View(profesorViewModel);
        }

        // GET: Profesores/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var profesor = await _context.Profesor
                .Include(p => p.IdGradosSeccionesNavigation)
                .Include(p => p.IdPersonaNavigation)
                .SingleOrDefaultAsync(m => m.IdProfesor == id);
            if (profesor == null)
            {
                return NotFound();
            }

            return View(profesor);
        }

        // POST: Profesores/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var profesor = await _context.Profesor.SingleOrDefaultAsync(m => m.IdProfesor == id);
            profesor.Estado = "0";
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool ProfesorExists(string id)
        {
            return _context.Profesor.Any(e => e.IdProfesor == id);
        }
    }
}