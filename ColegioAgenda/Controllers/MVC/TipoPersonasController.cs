using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ColegioAgenda.Models;
using ColegioAgenda.Clases;
using Microsoft.AspNetCore.Authorization;

namespace ColegioAgenda.Controllers.MVC
{
    [RequireHttps]
    [Authorize]
    public class TipoPersonasController : Controller
    {
        private readonly ColegioDBContext _context;
        private readonly Generador generador;

        public TipoPersonasController(ColegioDBContext context)
        {
            _context = context;
            generador = new Generador(context);
        }

        // GET: TipoPersonas
        public async Task<IActionResult> Index()
        {
            return View(await _context.TipoPersonas.Where(x => x.Estado == "1").ToListAsync());
        }

        // GET: TipoPersonas/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tipoPersonas = await _context.TipoPersonas
                .SingleOrDefaultAsync(m => m.IdTipoPersona == id);
            if (tipoPersonas == null)
            {
                return NotFound();
            }

            return View(tipoPersonas);
        }

        // GET: TipoPersonas/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: TipoPersonas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdTipoPersona,Descripcion,Estado")] TipoPersonas tipoPersonas)
        {
            if (ModelState.IsValid)
            {
                tipoPersonas.Estado = "1";
                tipoPersonas.IdTipoPersona = generador.GenerateIdAsync("TP", "TipoPersonas").Result;
                _context.Add(tipoPersonas);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(tipoPersonas);
        }

        // GET: TipoPersonas/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tipoPersonas = await _context.TipoPersonas.SingleOrDefaultAsync(m => m.IdTipoPersona == id);
            if (tipoPersonas == null)
            {
                return NotFound();
            }
            return View(tipoPersonas);
        }

        // POST: TipoPersonas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("IdTipoPersona,Descripcion,Estado")] TipoPersonas tipoPersonas)
        {
            if (id != tipoPersonas.IdTipoPersona)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(tipoPersonas);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TipoPersonasExists(tipoPersonas.IdTipoPersona))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            return View(tipoPersonas);
        }

        // GET: TipoPersonas/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tipoPersonas = await _context.TipoPersonas
                .SingleOrDefaultAsync(m => m.IdTipoPersona == id);
            if (tipoPersonas == null)
            {
                return NotFound();
            }

            return View(tipoPersonas);
        }

        // POST: TipoPersonas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var tipoPersonas = await _context.TipoPersonas.SingleOrDefaultAsync(m => m.IdTipoPersona == id);
            tipoPersonas.Estado = "0";
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool TipoPersonasExists(string id)
        {
            return _context.TipoPersonas.Any(e => e.IdTipoPersona == id);
        }
    }
}
