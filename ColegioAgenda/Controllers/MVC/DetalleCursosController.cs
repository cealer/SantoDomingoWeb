using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ColegioAgenda.Models;
using ColegioAgenda.Clases;
using Microsoft.AspNetCore.Authorization;

namespace ColegioAgenda.Controllers.MVC
{
    [RequireHttps]
    [Authorize]
    public class DetalleCursosController : Controller
    {
        private readonly ColegioDBContext _context;
        private readonly Generador generador;

        public DetalleCursosController(ColegioDBContext context)
        {
            _context = context;
            generador = new Generador(context);
        }

        // GET: DetalleCursos
        public async Task<IActionResult> Index()
        {
            var colegioDBContext = _context.DetalleCursos.Where(x=>x.Estado=="1").Include(d => d.IdAnioEscolarNavigation).Include(d => d.IdCursoNavigation).Include(d => d.IdGradosSeccionesNavigation);
            return View(await colegioDBContext.ToListAsync());
        }

        // GET: DetalleCursos/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var detalleCursos = await _context.DetalleCursos
                .Include(d => d.IdAnioEscolarNavigation)
                .Include(d => d.IdCursoNavigation)
                .Include(d => d.IdGradosSeccionesNavigation)
                .SingleOrDefaultAsync(m => m.IdDetalleCursos == id);
            if (detalleCursos == null)
            {
                return NotFound();
            }

            return View(detalleCursos);
        }

        // GET: DetalleCursos/Create
        public IActionResult Create()
        {
            ViewData["IdAnioEscolar"] = new SelectList(_context.AnioEscolar.Where(x => x.Estado == "1"), "IdAnioEscolar", "Anio");
            ViewData["IdCurso"] = new SelectList(_context.Cursos.Where(x => x.Estado == "1"), "IdCurso", "Descripcion");
            ViewData["IdGradosSecciones"] = new SelectList(_context.DetalleGradosSecciones.Where(x => x.Estado == "1").Include(x => x.IdGradoNavigation).Include(x => x.IdSeccionNavigation).Include(x=>x.IdAnioEscolarNavigation), "IdGradosSecciones", "Descripcion");
            return View();
        }

        // POST: DetalleCursos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdDetalleCursos,IdCurso,IdGradosSecciones,IdAnioEscolar")] DetalleCursos detalleCursos)
        {
            if (ModelState.IsValid)
            {
                detalleCursos.Estado = "1";
                detalleCursos.IdDetalleCursos = generador.GenerateIdAsync("DC","Detalle_Cursos").Result;
                _context.Add(detalleCursos);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewData["IdAnioEscolar"] = new SelectList(_context.AnioEscolar.Where(x => x.Estado == "1"), "IdAnioEscolar", "Anio", detalleCursos.IdAnioEscolar);
            ViewData["IdCurso"] = new SelectList(_context.Cursos.Where(x => x.Estado == "1"), "IdCurso", "Descripcion", detalleCursos.IdCurso);
            ViewData["IdGradosSecciones"] = new SelectList(_context.DetalleGradosSecciones.Where(x => x.Estado == "1").Include(x => x.IdGradoNavigation).Include(x => x.IdSeccionNavigation).Include(x => x.IdAnioEscolarNavigation), "IdGradosSecciones", "Descripcion", detalleCursos.IdGradosSecciones);
            return View(detalleCursos);
        }

        // GET: DetalleCursos/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var detalleCursos = await _context.DetalleCursos.SingleOrDefaultAsync(m => m.IdDetalleCursos == id);
            if (detalleCursos == null)
            {
                return NotFound();
            }
            ViewData["IdAnioEscolar"] = new SelectList(_context.AnioEscolar.Where(x => x.Estado == "1"), "IdAnioEscolar", "Anio", detalleCursos.IdAnioEscolar);
            ViewData["IdCurso"] = new SelectList(_context.Cursos.Where(x => x.Estado == "1"), "IdCurso", "Descripcion", detalleCursos.IdCurso);
            ViewData["IdGradosSecciones"] = new SelectList(_context.DetalleGradosSecciones.Where(x => x.Estado == "1").Include(x => x.IdGradoNavigation).Include(x => x.IdSeccionNavigation).Include(x => x.IdAnioEscolarNavigation), "IdGradosSecciones", "Descripcion", detalleCursos.IdGradosSecciones);
            return View(detalleCursos);
        }

        // POST: DetalleCursos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("IdDetalleCursos,IdCurso,IdGradosSecciones,IdAnioEscolar")] DetalleCursos detalleCursos)
        {
            if (id != detalleCursos.IdDetalleCursos)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(detalleCursos);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DetalleCursosExists(detalleCursos.IdDetalleCursos))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            ViewData["IdAnioEscolar"] = new SelectList(_context.AnioEscolar.Where(x=>x.Estado=="1"), "IdAnioEscolar", "Anio", detalleCursos.IdAnioEscolar);
            ViewData["IdCurso"] = new SelectList(_context.Cursos.Where(x => x.Estado == "1"), "IdCurso", "Descripcion", detalleCursos.IdCurso);
            ViewData["IdGradosSecciones"] = new SelectList(_context.DetalleGradosSecciones.Where(x => x.Estado == "1").Include(x => x.IdGradoNavigation).Include(x => x.IdSeccionNavigation).Include(x => x.IdAnioEscolarNavigation), "IdGradosSecciones", "Descripcion", detalleCursos.IdGradosSecciones);
            return View(detalleCursos);
        }

        // GET: DetalleCursos/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var detalleCursos = await _context.DetalleCursos
                .Include(d => d.IdAnioEscolarNavigation)
                .Include(d => d.IdCursoNavigation)
                .Include(d => d.IdGradosSeccionesNavigation)
                .SingleOrDefaultAsync(m => m.IdDetalleCursos == id);
            if (detalleCursos == null)
            {
                return NotFound();
            }

            return View(detalleCursos);
        }

        // POST: DetalleCursos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var detalleCursos = await _context.DetalleCursos.SingleOrDefaultAsync(m => m.IdDetalleCursos == id);
            detalleCursos.Estado ="0";
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool DetalleCursosExists(string id)
        {
            return _context.DetalleCursos.Any(e => e.IdDetalleCursos == id);
        }
    }
}
