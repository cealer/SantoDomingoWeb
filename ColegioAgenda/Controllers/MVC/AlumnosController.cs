using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ColegioAgenda.Models;
using ColegioAgenda.Models.ViewModel;
using ColegioAgenda.Clases;
using Microsoft.AspNetCore.Authorization;

namespace ColegioAgenda.Controllers.MVC
{
    [RequireHttps]
    [Authorize]
    public class AlumnosController : Controller
    {
        private readonly ColegioDBContext _context;
        private readonly Helper_Sexo helper_Sexo;
        private readonly Generador generador;

        public AlumnosController(ColegioDBContext context)
        {
            _context = context;
            helper_Sexo = new Helper_Sexo();
            generador = new Generador(context);
        }

        // GET: Alumnos
        public async Task<IActionResult> Index()
        {
            var colegioDBContext = _context.Alumno.Where(x => x.Estado == "1").Include(a => a.IdGradosSeccionesNavigation).Include(x => x.IdGradosSeccionesNavigation.IdAnioEscolarNavigation).Include(x => x.IdGradosSeccionesNavigation.IdGradoNavigation).Include(x => x.IdGradosSeccionesNavigation.IdSeccionNavigation).Include(a => a.IdPersonaNavigation);
            return View(await colegioDBContext.ToListAsync());
        }

        // GET: Alumnos/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var alumno = await _context.Alumno
                .Include(a => a.IdGradosSeccionesNavigation)
                .Include(a => a.IdPersonaNavigation)
                .SingleOrDefaultAsync(m => m.IdAlumno == id);
            if (alumno == null)
            {
                return NotFound();
            }

            return View(alumno);
        }

        // GET: Alumnos/Create
        public IActionResult Create()
        {
            ViewData["IdGradosSecciones"] = new SelectList(_context.DetalleGradosSecciones.Where(x => x.Estado == "1").Include(x => x.IdGradoNavigation).Include(x => x.IdSeccionNavigation).Include(x => x.IdAnioEscolarNavigation), "IdGradosSecciones", "Descripcion");
            ViewData["Sexo"] = new SelectList(helper_Sexo.Sexos(), "Valor", "Descripcion");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(AlumnoViewModel alumnoViewModel)
        {
            if (ModelState.IsValid)
            {

                alumnoViewModel.Persona.IdPersona = generador.GenerateIdAsync("PE", "Personas").Result;
                alumnoViewModel.Alumno.IdAlumno = generador.GenerateIdAsync("AL", "Alumno").Result;

                var trasaction = _context.Database.BeginTransaction();
                try
                {
                    alumnoViewModel.Persona.Estado = "1";
                    _context.Personas.Add(alumnoViewModel.Persona);
                    alumnoViewModel.Alumno.Estado = "1";
                    alumnoViewModel.Alumno.IdPersona = alumnoViewModel.Persona.IdPersona;
                    _context.Alumno.Add(alumnoViewModel.Alumno);
                    await _context.SaveChangesAsync();
                    trasaction.Commit();
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    trasaction.Rollback();
                }
            }
            ViewData["IdGradosSecciones"] = new SelectList(_context.DetalleGradosSecciones.Where(x => x.Estado == "1").Include(x => x.IdGradoNavigation).Include(x => x.IdSeccionNavigation).Include(x => x.IdAnioEscolarNavigation), "IdGradosSecciones", "Descripcion");
            ViewData["Sexo"] = new SelectList(helper_Sexo.Sexos(), "Valor", "Descripcion");
            return View(alumnoViewModel);
        }

        // GET: Alumnos/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }
            AlumnoViewModel alumnoView = new AlumnoViewModel();
            var alumno = await _context.Alumno.SingleOrDefaultAsync(m => m.IdAlumno == id);
            var persona = await _context.Personas.SingleOrDefaultAsync(m => m.IdPersona == alumno.IdPersona);
            alumnoView.Alumno = alumno;
            alumnoView.Persona = persona;
            if (alumno == null)
            {
                return NotFound();
            }
            ViewData["IdGradosSecciones"] = new SelectList(_context.DetalleGradosSecciones.Where(x => x.Estado == "1").Include(x => x.IdGradoNavigation).Include(x => x.IdSeccionNavigation).Include(x => x.IdAnioEscolarNavigation), "IdGradosSecciones", "Descripcion");
            ViewData["Sexo"] = new SelectList(helper_Sexo.Sexos(), "Valor", "Descripcion");
            return View(alumnoView);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, AlumnoViewModel alumnoViewModel)
        {
            if (id != alumnoViewModel.Alumno.IdAlumno)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(alumnoViewModel.Persona);
                    _context.Update(alumnoViewModel.Alumno);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AlumnoExists(alumnoViewModel.Alumno.IdAlumno))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            ViewData["IdGradosSecciones"] = new SelectList(_context.DetalleGradosSecciones.Where(x => x.Estado == "1").Include(x => x.IdGradoNavigation).Include(x => x.IdSeccionNavigation).Include(x => x.IdAnioEscolarNavigation), "IdGradosSecciones", "Descripcion");
            ViewData["Sexo"] = new SelectList(helper_Sexo.Sexos(), "Valor", "Descripcion");
            return View(alumnoViewModel);
        }

        // GET: Alumnos/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var alumno = await _context.Alumno
                .Include(a => a.IdGradosSeccionesNavigation)
                .Include(a => a.IdPersonaNavigation)
                .SingleOrDefaultAsync(m => m.IdAlumno == id);
            if (alumno == null)
            {
                return NotFound();
            }

            return View(alumno);
        }

        // POST: Alumnos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var alumno = await _context.Alumno.SingleOrDefaultAsync(m => m.IdAlumno == id);
            var persona = await _context.Personas.SingleOrDefaultAsync(m => m.IdPersona == alumno.IdPersona);
            alumno.Estado = "0";
            persona.Estado = "0";
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool AlumnoExists(string id)
        {
            return _context.Alumno.Any(e => e.IdAlumno == id);
        }
    }
}
