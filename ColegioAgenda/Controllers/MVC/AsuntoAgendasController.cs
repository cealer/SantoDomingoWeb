using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ColegioAgenda.Models;
using ColegioAgenda.Clases;
using ColegioAgenda.Models.ViewModel;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System.Security.Claims;
using ColegioAgenda.Azure.B2C;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Authorization;
using ColegioAgenda.Azure.NotificationHub;
using ColegioAgenda.Models.API;
using System.Net.Http;
using System.Text;

namespace ColegioAgenda.Controllers.MVC
{
    [RequireHttps]
    [Authorize]
    public class AsuntoAgendasController : Controller
    {
        private readonly ColegioDBContext _context;
        private readonly Generador generador;
        private readonly b2cClienteModel b2cclient;
        public B2CGraphClient b2CGraphClient { get; set; }
        private readonly OptionNotification optionNotification;

        public AsuntoAgendasController(ColegioDBContext context, IOptions<b2cClienteModel> b2coption, IOptions<OptionNotification> huboptions)
        {
            _context = context;
            generador = new Generador(context);
            b2cclient = b2coption.Value;
            b2CGraphClient = new B2CGraphClient(b2cclient);
            optionNotification = huboptions.Value;
        }

        public IEnumerable<Etiqueta> ObtenerEtiquetas()
        {
            return _context.Etiqueta.Where(x => x.Estado == "1");
        }

        public IEnumerable<Etiqueta> BuscarEtiquetas(string descripcion)
        {
            if (descripcion == null)
            {
                return _context.Etiqueta.Where(x => x.Estado == "1");
            }
            return _context.Etiqueta.Where(x => x.Estado == "1" && x.Descripcion.StartsWith(descripcion));
        }

        [HttpGet]
        public IActionResult BuscarAlumno()
        {
            var r = _context.Alumno.Where(x => x.Estado == "1").Include(x => x.IdPersonaNavigation).Include(x => x.IdGradosSeccionesNavigation);

            return Json(r);
        }

        [HttpGet]
        public IActionResult BuscarAlumnoNombre(string datos)
        {
            var r = _context.Alumno.Where(x => x.Estado == "1").Include(x => x.IdPersonaNavigation).
                Include(x => x.IdGradosSeccionesNavigation).Where(x => x.IdPersonaNavigation.Datos.StartsWith(datos));

            return Json(r);
        }

        public GettingStarted BuscarPadreFamilia(string matricula)
        {
            //matricula = "23123123";

            var r = b2CGraphClient.GetAllUsers($"$filter=extension_08869fcc1e434adba3abfd07cee85b2d_Matricula%20eq%20'{matricula}'").Result;

            return JsonConvert.DeserializeObject<GettingStarted>(r);

        }

        public async Task<IActionResult> Index()
        {
            var colegioDBContext = _context.AsuntoAgenda.Include(a => a.IdAlumnoNavigation).Include(x => x.IdAlumnoNavigation.IdPersonaNavigation);
            return View(await colegioDBContext.ToListAsync());
        }

        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var asuntoAgenda = await _context.AsuntoAgenda
                .Include(a => a.IdAlumnoNavigation).Include(x => x.IdAlumnoNavigation.IdPersonaNavigation).Include(x => x.IdAlumnoNavigation.IdGradosSeccionesNavigation.IdGradoNavigation).Include(x => x.IdAlumnoNavigation.IdGradosSeccionesNavigation.IdSeccionNavigation)
                .SingleOrDefaultAsync(m => m.IdAsuntoAgenda == id);
            if (asuntoAgenda == null)
            {
                return NotFound();
            }

            return View(asuntoAgenda);
        }


        public IActionResult Create()
        {

            ViewData["IdAlumno"] = new SelectList(_context.Alumno, "IdAlumno", "IdAlumno");
            ViewData["Etiquetas"] = _context.Etiqueta.Where(x => x.Estado == "1").ToList();
            return View();
        }

        [HttpGet]
        public IActionResult AgregarEtiqueta(string etiqueta)
        {
            if (etiqueta != null)
            {
                var _etiqueta = _context.Etiqueta.Where(x => x.IdEtiqueta == etiqueta).FirstOrDefault();

                var lista = Clases.SessionExtensions.Get<List<Etiqueta>>(HttpContext.Session, "Lista");

                if (lista == null)
                {
                    lista = new List<Etiqueta>();
                    lista.Add(_etiqueta);
                    Clases.SessionExtensions.Set<List<Etiqueta>>(HttpContext.Session, "Lista", lista);
                }
                else
                {
                    lista.Add(_etiqueta);
                    Clases.SessionExtensions.Set<List<Etiqueta>>(HttpContext.Session, "Lista", lista);
                }

                return Ok(_etiqueta);
            }

            return Ok();
        }

        [HttpGet]
        public IActionResult AgregarPersona(string id, string datos)
        {
            if (id != null)
            {
                var lista = Clases.SessionExtensions.Get<List<Emisor>>(HttpContext.Session, "ListaPersona");

                if (lista == null)
                {
                    lista = new List<Emisor>();
                    lista.Add(new Emisor { Id = id, Datos = datos });
                    Clases.SessionExtensions.Set<List<Emisor>>(HttpContext.Session, "ListaPersona", lista);
                }
                else
                {
                    lista.Add(new Emisor { Id = id, Datos = datos });
                    Clases.SessionExtensions.Set<List<Emisor>>(HttpContext.Session, "ListaPersona", lista);
                }

                return Json(datos);
            }

            return Ok();
        }

        [HttpGet]
        public IActionResult QuitarEtiqueta(string etiqueta)
        {

            if (etiqueta != null)
            {
                var _etiqueta = _context.Etiqueta.Where(x => x.IdEtiqueta == etiqueta).FirstOrDefault();
                var lista = Clases.SessionExtensions.Get<List<Etiqueta>>(HttpContext.Session, "Lista");

                if (lista == null)
                {
                    lista.Remove(_etiqueta);
                    Clases.SessionExtensions.Set<List<Etiqueta>>(HttpContext.Session, "Lista", lista);
                }

            }

            return Ok();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(AgendaViewModel agendaViewModel)
        {
            if (ModelState.IsValid)
            {
                //var trasaction = _context.Database.BeginTransaction();

                try
                {
                    var users = new List<string>();
                    var listaEnviar = Clases.SessionExtensions.Get<List<Emisor>>(HttpContext.Session, "ListaPersona");

                    if (listaEnviar != null)
                    {
                        foreach (var item in listaEnviar)
                        {
                            users.Add(item.Id);
                            agendaViewModel.AsuntoAgenda.IdAsuntoAgenda = generador.GenerateIdAsync("SA", "Asunto_Agenda").Result;
                            agendaViewModel.AsuntoAgenda.IdEmisor = User.FindFirst("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier")?.Value;
                            agendaViewModel.AsuntoAgenda.Emisor = User.FindFirst("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/givenname")?.Value + " " + User.FindFirst("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/surname")?.Value;
                            agendaViewModel.AsuntoAgenda.Visto = false;
                            agendaViewModel.AsuntoAgenda.Estado = "1";
                            agendaViewModel.AsuntoAgenda.IdReceptor = item.Id;
                            agendaViewModel.AsuntoAgenda.Receptor = item.Datos;
                            agendaViewModel.AsuntoAgenda.Fecha = DateTime.Now;
                            _context.Add(agendaViewModel.AsuntoAgenda);
                            await _context.SaveChangesAsync();

                        }

                        //Agregando etiquetas
                        var lista = Clases.SessionExtensions.Get<List<Etiqueta>>(HttpContext.Session, "Lista");
                        if (lista != null)
                        {
                            foreach (var item2 in lista)
                            {
                                var idDetalleAsunto = generador.GenerateIdAsync("DA", "Detalle_Asunto").Result;
                                var detalle = _context.DetalleAsunto.Add(new DetalleAsunto
                                {
                                    Estado = "1",
                                    IdDetalleAsunto = idDetalleAsunto,
                                    IdEtiqueta = item2.IdEtiqueta,
                                    IdAsuntoAgenda = agendaViewModel.AsuntoAgenda.IdAsuntoAgenda
                                });
                            }
                            await _context.SaveChangesAsync();
                        }

                        //Enviar notificacion al celular

                        var descripcion = "";
                        if (agendaViewModel.AsuntoAgenda.Descripcion.Length >= 50)
                        {
                            descripcion = agendaViewModel.AsuntoAgenda.Descripcion.Substring(0, 50) + "... ver completo en la app.";
                        }
                        else
                        {
                            descripcion = agendaViewModel.AsuntoAgenda.Descripcion;
                        }

                        var mensaje = new Mensaje { Descripcion = agendaViewModel.AsuntoAgenda.Descripcion, Titulo = agendaViewModel.AsuntoAgenda.Titulo, Users = users };

                        var request = JsonConvert.SerializeObject(mensaje);
                        var content = new StringContent(request, Encoding.UTF8, "application/json");
                        var client = new HttpClient();
                        client.BaseAddress = new Uri("https://localhost:44300/");
                        var url = string.Format("{0}/{1}", "api", "SendNotification");
                        var response = await client.PostAsync(url, content);

                        Clases.SessionExtensions.Set<List<Etiqueta>>(HttpContext.Session, "Lista", null);
                        Clases.SessionExtensions.Set<List<Emisor>>(HttpContext.Session, "ListaPersona", null);
                        return RedirectToAction("Index");
                    }

                }
                catch (Exception)
                {
                    //trasaction.Rollback();
                }

            }
            ViewData["IdAlumno"] = new SelectList(_context.Alumno, "IdAlumno", "IdAlumno", agendaViewModel.AsuntoAgenda.IdAlumno);
            ViewData["Etiquetas"] = new SelectList(_context.Etiqueta, "IdEtiqueta", "Descripcion");
            return View(agendaViewModel);
        }

    }
}