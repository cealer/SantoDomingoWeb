using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ColegioAgenda.Models;
using ColegioAgenda.Clases;
using Microsoft.AspNetCore.Authorization;

namespace ColegioAgenda.Controllers.MVC
{
    [RequireHttps]
    [Authorize]
    public class GradosController : Controller
    {
        private readonly ColegioDBContext _context;
        private readonly Generador generador;

        public GradosController(ColegioDBContext context)
        {
            _context = context;
            generador = new Generador(context);
        }

        // GET: Grados
        public async Task<IActionResult> Index()
        {
            return View(await _context.Grados.Where(x=>x.Estado=="1").ToListAsync());
        }

        // GET: Grados/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var grados = await _context.Grados
                .SingleOrDefaultAsync(m => m.IdGrado == id);
            if (grados == null)
            {
                return NotFound();
            }

            return View(grados);
        }

        // GET: Grados/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Grados/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdGrado,Descripcion,Primaria,Secundaria,Estado")] Grados grados)
        {
            if (ModelState.IsValid)
            {
                grados.Estado = "1";
                grados.IdGrado = generador.GenerateIdAsync("GR","Grados").Result;
                _context.Add(grados);

                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(grados);
        }

        // GET: Grados/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var grados = await _context.Grados.SingleOrDefaultAsync(m => m.IdGrado == id);
            if (grados == null)
            {
                return NotFound();
            }
            return View(grados);
        }

        // POST: Grados/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("IdGrado,Descripcion,Primaria,Secundaria,Estado")] Grados grados)
        {
            if (id != grados.IdGrado)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(grados);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!GradosExists(grados.IdGrado))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            return View(grados);
        }

        // GET: Grados/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var grados = await _context.Grados
                .SingleOrDefaultAsync(m => m.IdGrado == id);
            if (grados == null)
            {
                return NotFound();
            }

            return View(grados);
        }

        // POST: Grados/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var grados = await _context.Grados.SingleOrDefaultAsync(m => m.IdGrado == id);
            grados.Estado = "0";
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool GradosExists(string id)
        {
            return _context.Grados.Any(e => e.IdGrado == id);
        }
    }
}
