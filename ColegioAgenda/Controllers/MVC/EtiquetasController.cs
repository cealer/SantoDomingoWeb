using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ColegioAgenda.Models;
using ColegioAgenda.Clases;
using Microsoft.AspNetCore.Authorization;

namespace ColegioAgenda.Controllers.MVC
{
    [RequireHttps]
    [Authorize]
    public class EtiquetasController : Controller
    {
        private readonly ColegioDBContext _context;
        private readonly Generador generador;

        public EtiquetasController(ColegioDBContext context)
        {
            _context = context;
            generador = new Generador(context);
        }

        // GET: Etiquetas
        public async Task<IActionResult> Index()
        {
            return View(await _context.Etiqueta.Where(x => x.Estado == "1").ToListAsync());
        }

        // GET: Etiquetas/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var etiqueta = await _context.Etiqueta
                .SingleOrDefaultAsync(m => m.IdEtiqueta == id);
            if (etiqueta == null)
            {
                return NotFound();
            }

            return View(etiqueta);
        }

        // GET: Etiquetas/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Etiquetas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdEtiqueta,Descripcion,Estado")] Etiqueta etiqueta)
        {
            if (ModelState.IsValid)
            {
                etiqueta.Estado = "1";
                etiqueta.IdEtiqueta = generador.GenerateIdAsync("ET", "Etiqueta").Result;
                _context.Add(etiqueta);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(etiqueta);
        }

        // GET: Etiquetas/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var etiqueta = await _context.Etiqueta.SingleOrDefaultAsync(m => m.IdEtiqueta == id);
            if (etiqueta == null)
            {
                return NotFound();
            }
            return View(etiqueta);
        }

        // POST: Etiquetas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("IdEtiqueta,Descripcion,Estado")] Etiqueta etiqueta)
        {
            if (id != etiqueta.IdEtiqueta)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(etiqueta);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!EtiquetaExists(etiqueta.IdEtiqueta))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            return View(etiqueta);
        }

        // GET: Etiquetas/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var etiqueta = await _context.Etiqueta
                .SingleOrDefaultAsync(m => m.IdEtiqueta == id);
            if (etiqueta == null)
            {
                return NotFound();
            }

            return View(etiqueta);
        }

        // POST: Etiquetas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var etiqueta = await _context.Etiqueta.SingleOrDefaultAsync(m => m.IdEtiqueta == id);
            etiqueta.Estado = "0";
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool EtiquetaExists(string id)
        {
            return _context.Etiqueta.Any(e => e.IdEtiqueta == id);
        }
    }
}
