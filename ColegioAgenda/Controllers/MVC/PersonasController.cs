using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ColegioAgenda.Models;
using ColegioAgenda.Clases;
using Microsoft.AspNetCore.Authorization;

namespace ColegioAgenda.Controllers.MVC
{
    [RequireHttps]
    [Authorize]
    public class PersonasController : Controller
    {
        private readonly ColegioDBContext _context;
        private readonly Generador generador;
        private readonly Helper_Sexo helperSexo;


        public PersonasController(ColegioDBContext context)
        {
            _context = context;
            generador = new Generador(context);
            helperSexo = new Helper_Sexo();
        }

        // GET: Personas
        public async Task<IActionResult> Index()
        {
            var colegioDBContext = _context.Personas.Where(x => x.Estado == "1").Include(p => p.IdTipoPersonaNavigation);
            return View(await colegioDBContext.ToListAsync());
        }

        // GET: Personas/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var personas = await _context.Personas
                .Include(p => p.IdTipoPersonaNavigation)
                .SingleOrDefaultAsync(m => m.IdPersona == id);
            if (personas == null)
            {
                return NotFound();
            }

            return View(personas);
        }

        // GET: Personas/Create
        public IActionResult Create()
        {
            ViewData["IdTipoPersona"] = new SelectList(_context.TipoPersonas.Where(x => x.Estado == "1"), "IdTipoPersona", "Descripcion");
            ViewData["Sexo"] = new SelectList(helperSexo.Sexos(), "Valor", "Descripcion");
            return View();
        }

        // POST: Personas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdPersona,Nombres,ApellidoP,ApellidoM,Dni,Sexo,Direccion,NumContacto,Estado,IdTipoPersona,FecNac")] Personas personas)
        {
            if (ModelState.IsValid)
            {
                personas.Estado = "1";
                personas.IdPersona = generador.GenerateIdAsync("PE", "Personas").Result;
                _context.Add(personas);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewData["IdTipoPersona"] = new SelectList(_context.TipoPersonas.Where(x => x.Estado == "1"), "IdTipoPersona", "Descripcion", personas.IdTipoPersona);
            ViewData["Sexo"] = new SelectList(helperSexo.Sexos(), "Valor", "Descripcion");
            return View(personas);
        }

        // GET: Personas/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var personas = await _context.Personas.SingleOrDefaultAsync(m => m.IdPersona == id);
            if (personas == null)
            {
                return NotFound();
            }
            ViewData["IdTipoPersona"] = new SelectList(_context.TipoPersonas.Where(x => x.Estado == "1"), "IdTipoPersona", "Descripcion", personas.IdTipoPersona);
            ViewData["Sexo"] = new SelectList(helperSexo.Sexos(), "Valor", "Descripcion");
            return View(personas);
        }

        // POST: Personas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("IdPersona,Nombres,ApellidoP,ApellidoM,Dni,Sexo,Direccion,NumContacto,Estado,IdTipoPersona,FecNac")] Personas personas)
        {
            if (id != personas.IdPersona)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(personas);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PersonasExists(personas.IdPersona))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            ViewData["IdTipoPersona"] = new SelectList(_context.TipoPersonas.Where(x => x.Estado == "1"), "IdTipoPersona", "Descripcion", personas.IdTipoPersona);
            ViewData["Sexo"] = new SelectList(helperSexo.Sexos(), "Valor", "Descripcion");
            return View(personas);
        }

        // GET: Personas/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var personas = await _context.Personas
                .Include(p => p.IdTipoPersonaNavigation)
                .SingleOrDefaultAsync(m => m.IdPersona == id);
            if (personas == null)
            {
                return NotFound();
            }

            return View(personas);
        }

        // POST: Personas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var personas = await _context.Personas.SingleOrDefaultAsync(m => m.IdPersona == id);
            personas.Estado = "1";
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool PersonasExists(string id)
        {
            return _context.Personas.Any(e => e.IdPersona == id);
        }
    }
}
