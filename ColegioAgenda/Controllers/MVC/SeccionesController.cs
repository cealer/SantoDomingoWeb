using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ColegioAgenda.Models;
using ColegioAgenda.Clases;
using Microsoft.AspNetCore.Authorization;

namespace ColegioAgenda.Controllers.MVC
{
    [RequireHttps]
    [Authorize]
    public class SeccionesController : Controller
    {
        private readonly ColegioDBContext _context;
        private readonly Generador generador;

        public SeccionesController(ColegioDBContext context)
        {
            _context = context;
            generador = new Generador(context);
        }

        // GET: Secciones
        public async Task<IActionResult> Index()
        {
            return View(await _context.Secciones.Where(x=>x.Estado=="1").ToListAsync());
        }

        // GET: Secciones/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var secciones = await _context.Secciones
                .SingleOrDefaultAsync(m => m.IdSeccion == id);
            if (secciones == null)
            {
                return NotFound();
            }

            return View(secciones);
        }

        // GET: Secciones/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Secciones/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdSeccion,Descripcion,Estado")] Secciones secciones)
        {
            if (ModelState.IsValid)
            {
                secciones.Estado = "1";
                secciones.IdSeccion = generador.GenerateIdAsync("SE","Secciones").Result;
                _context.Add(secciones);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(secciones);
        }

        // GET: Secciones/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var secciones = await _context.Secciones.SingleOrDefaultAsync(m => m.IdSeccion == id);
            if (secciones == null)
            {
                return NotFound();
            }
            return View(secciones);
        }

        // POST: Secciones/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("IdSeccion,Descripcion,Estado")] Secciones secciones)
        {
            if (id != secciones.IdSeccion)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(secciones);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SeccionesExists(secciones.IdSeccion))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            return View(secciones);
        }

        // GET: Secciones/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var secciones = await _context.Secciones
                .SingleOrDefaultAsync(m => m.IdSeccion == id);
            if (secciones == null)
            {
                return NotFound();
            }

            return View(secciones);
        }

        // POST: Secciones/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var secciones = await _context.Secciones.SingleOrDefaultAsync(m => m.IdSeccion == id);
            secciones.Estado = "0";
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool SeccionesExists(string id)
        {
            return _context.Secciones.Any(e => e.IdSeccion == id);
        }
    }
}
