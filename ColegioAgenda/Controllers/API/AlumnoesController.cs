using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ColegioAgenda.Models;
using ColegioAgenda.Models.API;
using Newtonsoft.Json;
using ColegioAgenda.Azure.B2C;
using Microsoft.Extensions.Options;

namespace ColegioAgenda.Controllers.API
{
    [Produces("application/json")]
    [Route("api/Alumnos")]
    public class AlumnoesController : Controller
    {
        private readonly ColegioDBContext _context;

        private readonly b2cClienteModel b2cclient;
        public B2CGraphClient b2CGraphClient { get; set; }

        public AlumnoesController(ColegioDBContext context, IOptions<b2cClienteModel> b2coption)
        {
            _context = context;
            b2cclient = b2coption.Value;
            b2CGraphClient = new B2CGraphClient(b2cclient);
        }

        [Route("{valor}")]
        [HttpGet("{valor}")]
        public Alumno Get([FromRoute] string valor)
        {
            var r = _context.Alumno.
                Include(x => x.IdPersonaNavigation).
                Include(x => x.IdGradosSeccionesNavigation).
                Where(x => x.Estado == "1" && x.IdPersonaNavigation.Datos.StartsWith(valor)).
                FirstOrDefault();
            return r;
        }

        [Route("PadreFamilia/{matricula}")]
        [HttpGet("PadreFamilia/{matricula}")]
        public List<PadreFamilia> BuscarPadreFamilia(string matricula)
        {
            List<PadreFamilia> padres = new List<PadreFamilia>();

            var r = b2CGraphClient.GetAllUsers($"$filter=extension_08869fcc1e434adba3abfd07cee85b2d_Matricula%20eq%20'{matricula}'").Result;

            var rs = JsonConvert.DeserializeObject<GettingStarted>(r);

            foreach (var item in rs.Value)
            {
                padres.Add(new PadreFamilia { Id = item.ObjectId, Nombres = item.GivenName, Apellidos = item.Surname });
            }

            return padres;
        }

        [Route("lista")]
        [HttpGet("lista")]
        public Alumno Lista()
        {
            var r = _context.Alumno.Where(x => x.Estado == "1" && x.IdGradosSeccionesNavigation.IdAnioEscolarNavigation.Anio == DateTime.Now.Year.ToString()).
                Include(x => x.IdPersonaNavigation).
                Include(x => x.IdGradosSeccionesNavigation).Include(x => x.IdGradosSeccionesNavigation.IdAnioEscolarNavigation).FirstOrDefault();
            return r;
        }

        [Route("grado/{id}")]
        [HttpGet("grado/{id}")]
        public DetalleGradosSecciones GradoAlumno([FromRoute] string id)
        {
            var r = _context.DetalleGradosSecciones.Where(x => x.IdGradosSecciones == id).
                Include(x => x.IdAnioEscolarNavigation).
                Include(x => x.IdGradoNavigation).
                FirstOrDefault();
            return r;
        }

        //[HttpGet]
        //public IEnumerable<AlumnoResponse> GetAlumnos([FromRoute] string Anio)
        //{
        //    var alumno = _context.Alumno.Include(x => x.IdPersonaNavigation).Include(x => x.IdGradosSeccionesNavigation).Include(x => x.IdGradosSeccionesNavigation.IdAnioEscolarNavigation).Where(x => x.Estado == "1" && x.IdGradosSeccionesNavigation.IdAnioEscolarNavigation.Anio == Anio);

        //    var response = new List<AlumnoResponse>();

        //    foreach (var item in alumno)
        //    {
        //        response.Add(new AlumnoResponse
        //        {
        //            ApellidoM = item.IdPersonaNavigation.ApellidoM,
        //            ApellidoP = item.IdPersonaNavigation.ApellidoP,
        //            Dni = item.IdPersonaNavigation.Dni,
        //            Estado = item.Estado,
        //            FecNac = item.IdPersonaNavigation.FecNac,
        //            Grado = item.IdGradosSeccionesNavigation.Descripcion,
        //            IdAlumno = item.IdAlumno,
        //            IdPersona = item.IdPersona,
        //            Nombres = item.IdPersonaNavigation.Nombres,
        //            Sexo = item.IdPersonaNavigation.Sexo
        //        });
        //    }

        //    return response;
        //}

    }
}