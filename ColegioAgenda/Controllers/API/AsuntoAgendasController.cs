using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ColegioAgenda.Models;
using ColegioAgenda.Clases;
using Microsoft.AspNetCore.Authorization;

namespace ColegioAgenda.Controllers.API
{
    [Produces("application/json")]
    [Route("api/AsuntoAgendas")]
    public class AsuntoAgendasController : Controller
    {
        private readonly ColegioDBContext _context;
        private readonly Generador generador;

        public AsuntoAgendasController(ColegioDBContext context)
        {
            _context = context;
            generador = new Generador(context);
        }

        [Route("listarRecibidas/{receptor}")]
        [HttpGet("listarRecibidas/{receptor}")]
        public IEnumerable<AsuntoAgenda> ObtenerRecibidas([FromRoute] string receptor)
        {
            var r = _context.AsuntoAgenda.Where(x => x.IdReceptor.StartsWith(receptor) && x.Estado == "1");
            return r;
        }

        [Route("listarEnviadas/{receptor}")]
        [HttpGet("listarEnviadas/{receptor}")]
        public IEnumerable<AsuntoAgenda> ObtenerEnviadas([FromRoute] string receptor)
        {
            var r = _context.AsuntoAgenda.Where(x => x.IdReceptor.StartsWith(receptor) && x.Estado == "1");
            return r;
        }

        //Notificaciones recibidas
        [Route("buscarRecibidas/{valor}/{valor2}")]
        [HttpGet("buscarRecibidas/{valor}/{valor2}")]
        public IEnumerable<AsuntoAgenda> BuscarRecibidas([FromRoute] string valor, [FromRoute] string valor2)
        {
            var r = _context.AsuntoAgenda.Where(x => x.IdReceptor.StartsWith(valor) && x.Titulo.StartsWith(valor2) && x.Estado == "1").Include(x => x.IdAlumnoNavigation).Include(x => x.IdAlumnoNavigation.IdPersonaNavigation);
            return r;
        }

        [Route("buscarEnviadas/{valor}/{valor2}")]
        [HttpGet("buscarEnviadas/{valor}/{valor2}")]
        //Notificaciones enviada
        public IEnumerable<AsuntoAgenda> BuscarEnviadas([FromRoute] string valor, [FromRoute] string valor2)
        {
            var r = _context.AsuntoAgenda.Where(x => x.IdEmisor.StartsWith(valor) && x.Titulo.StartsWith(valor2) && x.Estado == "1").Include(x => x.IdAlumnoNavigation).Include(x => x.IdAlumnoNavigation.IdPersonaNavigation);
            return r;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetAsuntoAgenda([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var asuntoAgenda = await _context.AsuntoAgenda.Include(x=>x.IdAlumnoNavigation).Include(x=>x.IdAlumnoNavigation.IdPersonaNavigation).SingleOrDefaultAsync(m => m.IdAsuntoAgenda == id);

            if (asuntoAgenda == null)
            {
                return NotFound();
            }

            return Ok(asuntoAgenda);
        }

        [HttpPost]
        public async Task<IActionResult> PostAsuntoAgenda([FromBody] AsuntoAgenda asuntoAgenda)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            asuntoAgenda.IdAsuntoAgenda = generador.GenerateIdAsync("SA", "Asunto_Agenda").Result;
            asuntoAgenda.Fecha = DateTime.Now;
            asuntoAgenda.Estado = "1";
            _context.AsuntoAgenda.Add(asuntoAgenda);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetAsuntoAgenda", new { id = asuntoAgenda.IdAsuntoAgenda }, asuntoAgenda);
        }

        [Route("Etiquetas")]
        [HttpPost]
        public async Task<IActionResult> PostEtiquetas([FromBody] List<Etiqueta> etiqueta)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Etiqueta.AddRange(etiqueta);
            await _context.SaveChangesAsync();

            return Ok();
        }
    }
}