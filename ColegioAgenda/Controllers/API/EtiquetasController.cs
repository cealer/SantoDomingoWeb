using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ColegioAgenda.Models;
using System.Security.Claims;
using System.Net.Http;
using System.Net;
using Microsoft.AspNetCore.Authorization;

namespace ColegioAgenda.Controllers.API
{
    [Produces("application/json")]
    [Route("api/Etiquetas")]
    public class EtiquetasController : Controller
    {
        private readonly ColegioDBContext _context;

        public EtiquetasController(ColegioDBContext context)
        {
            _context = context;
        }

        [Route("lista")]
        [HttpGet]
        public IEnumerable<Etiqueta> ObtenerEtiquetas()
        {
            //var lista = new List<Etiqueta> { new Etiqueta {Descripcion="asd",Estado="1",IdEtiqueta="ET000001" } };
            //return lista;
            return _context.Etiqueta.Where(x => x.Estado == "1");
        }

        //[Route("lista")]
        //[HttpGet]
        //public IActionResult ObtenerEtiquetas()
        //{
        //    List<Etiqueta> lista = new List<Etiqueta> { new Etiqueta { Descripcion = "asd", Estado = "1", IdEtiqueta = "ET00001" } };

        //    var scopes = HttpContext.User.FindFirst("http://schemas.microsoft.com/identity/claims/scope")?.Value;

        //    if (!string.IsNullOrEmpty(Startup.ScopeRead) && scopes != null

        //            && scopes.Split(' ').Any(s => s.Equals(Startup.ScopeRead)))

        //        return Ok(lista);

        //    else

        //        return Unauthorized();

        //    //return _context.Etiqueta.Where(x => x.Estado == "1");
        //}
        
        [Route("buscar/{valor}")]
        [HttpGet("{valor}")]
        public IEnumerable<Etiqueta> BuscarEtiquetas([FromRoute] string valor)
        {
            return _context.Etiqueta.Where(x => x.Estado == "1" && x.Descripcion.StartsWith(valor));
        }

        // POST: api/Etiquetas
        [HttpPost]
        public async Task<IActionResult> PostEtiqueta([FromBody] Etiqueta etiqueta)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Etiqueta.Add(etiqueta);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetEtiqueta", new { id = etiqueta.IdEtiqueta }, etiqueta);
        }

    }
}