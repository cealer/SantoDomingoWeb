﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ColegioAgenda.Azure.NotificationHub
{
    public class OptionNotification
    {
        public string ConnectionStrings { get; set; }
        public string HubName { get; set; }
    }
}
