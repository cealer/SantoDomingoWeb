﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ColegioAgenda.Azure.B2C
{
    public class b2cClienteModel
    {
        public string aadInstance { get; set; }
        public string aadGraphResourceId { get; set; }
        public string aadGraphEndpoint { get; set; }
        public string aadGraphSuffix { get; set; }
        public string aadGraphVersion { get; set; }

        public string ClientIdGraph { get; set; }
        public string Tenant { get; set; }
        public string ClientSecretGraph { get; set; }
    }
}