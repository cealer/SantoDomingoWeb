﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ColegioAgenda.Azure.B2C
{

    public partial class GettingStarted
    {
        [JsonProperty("odata.metadata")]
        public string OdataMetadata { get; set; }

        [JsonProperty("value")]
        public Value[] Value { get; set; }
    }

    public partial class Value
    {
        [JsonProperty("accountEnabled")]
        public bool AccountEnabled { get; set; }

        [JsonProperty("assignedLicenses")]
        public object[] AssignedLicenses { get; set; }

        [JsonProperty("assignedPlans")]
        public object[] AssignedPlans { get; set; }

        [JsonProperty("city")]
        public object City { get; set; }

        [JsonProperty("companyName")]
        public object CompanyName { get; set; }

        [JsonProperty("country")]
        public object Country { get; set; }

        [JsonProperty("creationType")]
        public string CreationType { get; set; }

        [JsonProperty("deletionTimestamp")]
        public object DeletionTimestamp { get; set; }

        [JsonProperty("department")]
        public object Department { get; set; }

        [JsonProperty("dirSyncEnabled")]
        public object DirSyncEnabled { get; set; }

        [JsonProperty("displayName")]
        public string DisplayName { get; set; }

        [JsonProperty("employeeId")]
        public object EmployeeId { get; set; }

        [JsonProperty("extension_08869fcc1e434adba3abfd07cee85b2d_Matricula")]
        public string Extension08869fcc1e434adba3abfd07cee85b2dMatricula { get; set; }

        [JsonProperty("facsimileTelephoneNumber")]
        public object FacsimileTelephoneNumber { get; set; }

        [JsonProperty("givenName")]
        public string GivenName { get; set; }

        [JsonProperty("immutableId")]
        public object ImmutableId { get; set; }

        [JsonProperty("isCompromised")]
        public object IsCompromised { get; set; }

        [JsonProperty("jobTitle")]
        public object JobTitle { get; set; }

        [JsonProperty("lastDirSyncTime")]
        public object LastDirSyncTime { get; set; }

        [JsonProperty("mail")]
        public object Mail { get; set; }

        [JsonProperty("mailNickname")]
        public string MailNickname { get; set; }

        [JsonProperty("mobile")]
        public object Mobile { get; set; }

        [JsonProperty("objectId")]
        public string ObjectId { get; set; }

        [JsonProperty("objectType")]
        public string ObjectType { get; set; }

        [JsonProperty("odata.type")]
        public string OdataType { get; set; }

        [JsonProperty("onPremisesDistinguishedName")]
        public object OnPremisesDistinguishedName { get; set; }

        [JsonProperty("onPremisesSecurityIdentifier")]
        public object OnPremisesSecurityIdentifier { get; set; }

        [JsonProperty("otherMails")]
        public object[] OtherMails { get; set; }

        [JsonProperty("passwordPolicies")]
        public string PasswordPolicies { get; set; }

        [JsonProperty("passwordProfile")]
        public object PasswordProfile { get; set; }

        [JsonProperty("physicalDeliveryOfficeName")]
        public object PhysicalDeliveryOfficeName { get; set; }

        [JsonProperty("postalCode")]
        public object PostalCode { get; set; }

        [JsonProperty("preferredLanguage")]
        public object PreferredLanguage { get; set; }

        [JsonProperty("provisionedPlans")]
        public object[] ProvisionedPlans { get; set; }

        [JsonProperty("provisioningErrors")]
        public object[] ProvisioningErrors { get; set; }

        [JsonProperty("proxyAddresses")]
        public object[] ProxyAddresses { get; set; }

        [JsonProperty("refreshTokensValidFromDateTime")]
        public string RefreshTokensValidFromDateTime { get; set; }

        [JsonProperty("showInAddressList")]
        public object ShowInAddressList { get; set; }

        [JsonProperty("signInNames")]
        public SignInName[] SignInNames { get; set; }

        [JsonProperty("sipProxyAddress")]
        public object SipProxyAddress { get; set; }

        [JsonProperty("state")]
        public object State { get; set; }

        [JsonProperty("streetAddress")]
        public string StreetAddress { get; set; }

        [JsonProperty("surname")]
        public string Surname { get; set; }

        [JsonProperty("telephoneNumber")]
        public object TelephoneNumber { get; set; }

        [JsonProperty("usageLocation")]
        public object UsageLocation { get; set; }

        [JsonProperty("userIdentities")]
        public object[] UserIdentities { get; set; }

        [JsonProperty("userPrincipalName")]
        public string UserPrincipalName { get; set; }

        [JsonProperty("userType")]
        public string UserType { get; set; }

        [JsonIgnore]
        public string Datos
        {
            get
            {
                return $"{Surname} {GivenName}";
            }
        }

    }

    public partial class SignInName
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("value")]
        public string Value { get; set; }
    }

    public partial class GettingStarted
    {
        public static GettingStarted FromJson(string json) => JsonConvert.DeserializeObject<GettingStarted>(json, Converter.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this GettingStarted self) => JsonConvert.SerializeObject(self, Converter.Settings);
    }

    public class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
        };
    }
}